package com.gmail.ol.bulhakov;

public class Main {

	public static void main(String[] args) {
		// Home work 6.1 Найти максимальное число
		int[] arr = new int[] { 2, 35, 11, 85, 12, 3, 36, 22, 48, 24, 33 };

		System.out.println("Самое большое число массива : " + getMax(arr));
	}

	public static int getMax(int[] a) {
		int maxNumber = a[0];
		for (int i = 0; i < a.length; i += 1) {
			if (a[i] > maxNumber) {
				maxNumber = a[i];
			}
		}
		return maxNumber;

	}

}
