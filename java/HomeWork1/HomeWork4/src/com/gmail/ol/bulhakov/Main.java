package com.gmail.ol.bulhakov;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// 1 Нарисовать обои

		int lines;
		Scanner sc = new Scanner(System.in);
		System.out.println("Введите количество строк");
		lines = sc.nextInt();

		for (int i = 1; i <= lines; i = i + 1) {
			for (int j = 1; j <= 7; j = j + 1) {
				if (j % 2 == 0) {
					System.out.print("+++");
				} else {
					System.out.print("***");
				}
			}
			System.out.println("");
		}

		// 2 Найти факториал

		int n;
		long factorial = 1;

		System.out.println("Для поиска факториала введите номер от 4 до 16");
		n = sc.nextInt();
		if (n < 4 || n > 16) {
			System.out.println("Вы ввели не правельное число оно или меньше 4 или больше 16!!!");
		} else {
			for (int i = 1; i <= n; i = i + 1) {
				factorial = factorial * i;
			}
			System.out.println("Факториал числа " + n + " равен : " + factorial);
		}

		// 3 Таблица умножения на 5

		int five = 5;
		int max = 10;
		for (int i = 1; i <= max; i = i + 1) {
			int sum = i * five;
			System.out.println(i + " X " + five + " = " + sum);
		}

		// 4 Прямоугольник из звездочек и пробелов

		int userH;
		int userL;

		System.out.println("Введите высоту");
		userH = sc.nextInt();
		System.out.println("Введите ширину");
		userL = sc.nextInt();

		for (int i = 1; i <= userH; i = i + 1) {
			for (int j = 1; j <= userL; j = j + 1) {
				if (i == 1 || i == userH || j == 1 || j == userL) {
					System.out.print("*");
				}

				else {
					System.out.print(" ");
				}
			}
			System.out.println("");
		}

		// 5 Простые числа

		for (int i = 2; i <= 100; i = i + 1) {
			boolean isSimpleNumber = true;
			for (int j = 2; j < i; j = j + 1) {
				if (i % j == 0) {
					isSimpleNumber = false;
					break;
				} else {

				}

			}
			if (isSimpleNumber) {
				System.out.print(i);
			}
		}

	}

}
