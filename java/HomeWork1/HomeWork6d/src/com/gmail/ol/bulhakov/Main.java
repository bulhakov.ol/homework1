package com.gmail.ol.bulhakov;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// Home work 6.5 Найти количество слов
		Scanner sc = new Scanner(System.in);
		System.out.println("Введите строку текста");
		String text = sc.nextLine();
		if(text.length() != 0 && text.charAt(0) != ' ') {
		int wordsCount = getWordsCount(text);
		System.out.println("Количество слов в строке :" + wordsCount);
		}
		else {
			System.out.println("Вы не ввели никаких символов или начали с пробела");
		}
		

	}
	public static int getWordsCount (String a) {
		
		
		int count = 1;
		
			for(int i =  0; i < a.length(); i+=1) {
				if(a.charAt(i) == ' ') {
					count +=1;
				}
			}
		
		
		return count;
	}

}
