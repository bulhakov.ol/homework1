package com.gmail.ol.bulhakov;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Задача 1

		Scanner scan = new Scanner (System.in);
		int number;
		System.out.println("Введите пятизначное число");
		number=scan.nextInt();
		int firstNumber = number / 10000;
		int secondNumber  = number % 10000 /1000;
		int thirdNumber = number % 1000 / 100;
		int forthNumber = number % 100 / 10;
		int fifthNumber = number % 10;
		
		System.out.println(firstNumber);
		System.out.println(secondNumber);
		System.out.println(thirdNumber);
		System.out.println(forthNumber);
		System.out.println(fifthNumber);
		
		
		
		// Задача 2
		
		double firstSide;
		double secondSide;
		double thirdSide;
		System.out.println("Введите длину первой стороны треугольника");
		firstSide = scan.nextDouble();
		System.out.println("Введите длину второй стороны треугольника");
		secondSide = scan.nextDouble();
		System.out.println("Введите длину третьей стороны треугольника");
		thirdSide = scan.nextDouble();
		
		double poluPerimetr = (firstSide + secondSide + thirdSide) / 2;
		
		double s = Math.sqrt(poluPerimetr*(poluPerimetr-firstSide)*(poluPerimetr-secondSide)*(poluPerimetr-thirdSide));
		
		System.out.println("Площадь треугольника S = " + s);
		
		
		
		// Задача 3
		
		double radius;
		
		System.out.println("Введите радиус окружности");
		radius = scan.nextDouble();
		
		double l = 2 * Math.PI * radius;
		
		System.out.println("Длина окружности l = " + l);
		
		
	}

}
