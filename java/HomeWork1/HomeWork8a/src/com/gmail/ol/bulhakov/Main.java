package com.gmail.ol.bulhakov;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {

	public static void main(String[] args) {
		// Home work 8.2 Запись массива в текстовый файл
		
		int [][] arr = {{2,5,7},{3,1,6},{8,55,12}};
		File userText = new File ("arrFile.txt");
		arrToFile(arr, userText);
		

	}
	public static void arrToFile (int [][] array, File f) {
		try(PrintWriter pr = new PrintWriter(f)){
			for(int i = 0; i< array.length; i +=1 ) {
				for(int j = 0; j < array[i].length; j+=1) {
				
						pr.print(array [i] [j] + "\t");
					
				}
				pr.println();
			}
			
		}
		catch (IOException e) {
			System.out.println(e);
		}
	
		
	}

}
