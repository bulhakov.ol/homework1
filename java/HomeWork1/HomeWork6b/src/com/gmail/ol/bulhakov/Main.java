package com.gmail.ol.bulhakov;

public class Main {

	public static void main(String[] args) {
		// Home work 6.3 Прямоугольник из звездочек
		int sideOne = 8;
		int sideTwo = 24;
		drawing(sideOne, sideTwo);
	}

	public static void drawing(int a, int b) {
		for (int i = 1; i <= a; i += 1) {
			for (int j = 1; j <= b; j += 1) {
				System.out.print("*");
			}
			System.out.println("");
		}

	}

}
