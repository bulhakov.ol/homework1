package com.gmail.ol.bulhakov;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		// Home Work 6.4 Поиск элемента
		
		Scanner sc = new Scanner(System.in);
		int[] arrOne = new int[5];
		for (int i = 0; i < arrOne.length; i += 1) {
			arrOne[i] = (int) (Math.random() * 12);
		}
		int userNumber;

		System.out.println("Введите число");
		userNumber = sc.nextInt();
		int resultNumber = getResult(arrOne, userNumber);

		System.out.println(Arrays.toString(arrOne));
		System.out.println("Результат поиска : " + resultNumber);
	}

	public static int getResult(int[] a, int b) {
		int index = -1;
		for (int i = 0; i < a.length; i += 1) {
			if (b == a[i]) {
				index = i;
			}

		}
		return index;
	}

}
