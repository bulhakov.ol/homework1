package com.gmail.ol.bulhakov;

import java.util.Formatter;

public class Main {

	public static void main(String[] args) {
		// Home work 7.4 Вариации числа Пи 
		
		Formatter fr = new Formatter();
		
		fr.format("%.2f", Math.PI);
		String text1= fr.toString();
		System.out.println("Два числа после запятой : " +text1);
		
		String text2 = String.format("%.3f", Math.PI);
		System.out.println("Три числа после запятой : "+text2);
		
		String text3 = String.format("%.4f", Math.PI);
		System.out.println("Четыри числа после запятой : "+text3);
		

	}

}
