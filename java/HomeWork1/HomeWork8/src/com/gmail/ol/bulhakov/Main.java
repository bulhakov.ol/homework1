package com.gmail.ol.bulhakov;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// Home work 8.1 Консольный текстовый редактор
		
		Scanner sc = new Scanner (System.in);
		StringBuilder sb = new StringBuilder();
		String confirm;
		File userText = new File("userText.txt");
		System.out.println("Введите текст");
		sb.append(sc.nextLine());
		String text =sb.toString();
		
		System.out.println("Сохранить введите Y - Да, N - Нет");
		confirm = sc.nextLine();
		char [] confArr = confirm.toCharArray();
		
		if(confArr.length == 1 && confArr[0] == 'Y' || confArr.length == 1 && confArr[0] == 'y') {
			writer(text, userText);
		}
		
		else if (confArr.length == 1 && confArr[0] == 'N' || confArr.length == 1 && confArr[0] == 'n') {
			System.out.println("Текст не сохранен");
			userText.delete();
		}
		
		else {
			System.out.println("Вы ввели неправельное значение");
		}
	}
	public static void writer (String x, File y) {

		try (PrintWriter a = new PrintWriter(y)){
			a.println(x);
			System.out.println("Вы успешно сохранили этот текст: " + x);
		} catch (IOException e) {
			System.out.println(e);
		}
		
		}
	}


