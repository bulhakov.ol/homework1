package com.gmail.ol.bulhakov;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// homework 2.1 выбрать самое большое число из списка

		Scanner sc = new Scanner(System.in);

		int number1;
		int number2;
		int number3;
		int number4;
		int maxNumber;

		System.out.println("Введите первое число");
		number1 = sc.nextInt();
		System.out.println("Введите второе число");
		number2 = sc.nextInt();
		System.out.println("Введите третье число");
		number3 = sc.nextInt();
		System.out.println("Введите четвертое число");
		number4 = sc.nextInt();

		maxNumber = number1;

		if (maxNumber < number2) {
			maxNumber = number2;
		} else {

		}
		if (maxNumber < number3) {
			maxNumber = number3;
		} else {

		}
		if (maxNumber < number4) {
			maxNumber = number4;
		} else {

		}

		System.out.println("Самое большое число: " + maxNumber);

		// homework 2.2 по номеру квартиры определить этаж и подьезд

		int apartmentNumber;

		System.out.println("Введите номер квартиры");
		apartmentNumber = sc.nextInt();

		if (apartmentNumber <= 4) {
			System.out.println("1 этаж 1 подъезд");
		} else if (apartmentNumber <= 8) {
			System.out.println("2 этаж 1 подъезд");
		} else if (apartmentNumber <= 12) {
			System.out.println("3 этаж 1 подъезд");
		} else if (apartmentNumber <= 16) {
			System.out.println("4 этаж 1 подъезд");
		} else if (apartmentNumber <= 20) {
			System.out.println("5 этаж 1 подъезд");
		} else if (apartmentNumber <= 24) {
			System.out.println("6 этаж 1 подъезд");
		} else if (apartmentNumber <= 28) {
			System.out.println("7 этаж 1 подъезд");
		} else if (apartmentNumber <= 32) {
			System.out.println("8 этаж 1 подъезд");
		} else if (apartmentNumber <= 36) {
			System.out.println("9 этаж 1 подъезд");
		}

		else if (apartmentNumber <= 40) {
			System.out.println("1 этаж 2 подъезд");
		} else if (apartmentNumber <= 44) {
			System.out.println("2 этаж 2 подъезд");
		} else if (apartmentNumber <= 48) {
			System.out.println("3 этаж 2 подъезд");
		} else if (apartmentNumber <= 52) {
			System.out.println("4 этаж 2 подъезд");
		} else if (apartmentNumber <= 56) {
			System.out.println("5 этаж 2 подъезд");
		} else if (apartmentNumber <= 60) {
			System.out.println("6 этаж 2 подъезд");
		} else if (apartmentNumber <= 64) {
			System.out.println("7 этаж 2 подъезд");
		} else if (apartmentNumber <= 68) {
			System.out.println("8 этаж 2 подъезд");
		} else if (apartmentNumber <= 72) {
			System.out.println("9 этаж 2 подъезд");

		}

		else if (apartmentNumber <= 76) {
			System.out.println("1 этаж 3 подъезд");
		} else if (apartmentNumber <= 80) {
			System.out.println("2 этаж 3 подъезд");
		} else if (apartmentNumber <= 84) {
			System.out.println("3 этаж 3 подъезд");
		} else if (apartmentNumber <= 88) {
			System.out.println("4 этаж 3 подъезд");
		} else if (apartmentNumber <= 92) {
			System.out.println("5 этаж 3 подъезд");
		} else if (apartmentNumber <= 96) {
			System.out.println("6 этаж 3 подъезд");
		} else if (apartmentNumber <= 100) {
			System.out.println("7 этаж 3 подъезд");
		} else if (apartmentNumber <= 104) {
			System.out.println("8 этаж 3 подъезд");
		} else if (apartmentNumber <= 108) {
			System.out.println("9 этаж 2 подъезд");

		} else if (apartmentNumber <= 112) {
			System.out.println("1 этаж 4 подъезд");
		} else if (apartmentNumber <= 116) {
			System.out.println("2 этаж 4 подъезд");
		} else if (apartmentNumber <= 120) {
			System.out.println("3 этаж 4 подъезд");
		} else if (apartmentNumber <= 124) {
			System.out.println("4 этаж 4 подъезд");
		} else if (apartmentNumber <= 128) {
			System.out.println("5 этаж 4 подъезд");
		} else if (apartmentNumber <= 132) {
			System.out.println("6 этаж 4 подъезд");
		} else if (apartmentNumber <= 136) {
			System.out.println("7 этаж 4 подъезд");
		} else if (apartmentNumber <= 140) {
			System.out.println("8 этаж 4 подъезд");
		} else if (apartmentNumber <= 144) {
			System.out.println("9 этаж 4 подъезд");

		} else {
			System.out.println("Нет такой квартиры");
		}

		// homework 2.3 Определить количество дней в году который вводит пользователь

		int userYear;

		System.out.println("Введите год");
		userYear = sc.nextInt();

		if (userYear % 4 == 0 || userYear % 100 != 0 && userYear % 400 == 0) {

			System.out.println("Это высокосный год 366 дней");

		} else {
			System.out.println("Это не высокосный год 365 дней");
		}

		// homework 2.4 Определить существует треугольник или нет

		double a;
		double b;
		double c;
		System.out.println("Введите длину первой стороны");
		a = sc.nextDouble();
		System.out.println("Введите длину второй стороны");
		b = sc.nextDouble();
		System.out.println("Введите длину третьей стороны");
		c = sc.nextDouble();

		if (a + b > c && a + c > b && b + c > a) {
			System.out.println("Треугольник существует");
		} else {
			System.out.println("Треугольник не существует");
		}

		// homework 2.5 Пренадлежит ли точка кругу

		double xCordinate;
		double yCordinate;
		double hypotenuse;
		double radio = 4;

		System.out.println("Введите кординату Х ");
		xCordinate = sc.nextDouble();
		System.out.println("Введите кординату У ");
		yCordinate = sc.nextDouble();

		hypotenuse = Math.sqrt(Math.pow(xCordinate, 2) + Math.pow(yCordinate, 2));

		if (hypotenuse <= radio) {
			System.out.println("Точка  с кординатами " + xCordinate + " и " + yCordinate + " пренадлежит кругу");
		} else {
			System.out.println("Точка  с кординатами " + xCordinate + " и " + yCordinate + " не пренадлежит кругу");
		}

		// homework 2.6 Пренадлежит ли точка треугольнику

		double xA = 0;
		double yA = 0;
		double xB = 4;
		double yB = 4;
		double xC = 6;
		double yC = 1;

		System.out.println("Введите Х координату точки: ");
		double xP = sc.nextDouble();
		System.out.println("Введите Y координату точки: ");
		double yP = sc.nextDouble();

		double s1 = (xA - xP) * (yB - yA) - (xB - xA) * (yA - yP);
		double s2 = (xB - xP) * (yC - yB) - (xC - xB) * (yB - yP);
		double s3 = (xC - xP) * (yA - yC) - (xA - xC) * (yC - yP);

		if ((s1 >= 0 && s2 >= 0 && s3 >= 0) || (s1 <= 0 && s2 <= 0 && s3 <= 0)) {
			System.out.println("Точка принадлежит треугольнику");
		} else {
			System.out.println("Точка не принадлежит треугольнику");
		}

		// homework 2.7 является ли число счастливым билетом

		int happyNumber;
		int n1;
		int n2;
		int n3;
		int n4;

		System.out.println("Введите четырехзначное число чтобы узнать счастливый ли это билет");
		happyNumber = sc.nextInt();

		n1 = happyNumber / 1000;
		n2 = happyNumber % 1000 / 100;
		n3 = happyNumber % 100 / 10;
		n4 = happyNumber % 10;

		int sumN1N2 = n1 + n2;
		int sumN3N4 = n3 + n4;

		if (sumN1N2 == sumN3N4) {
			System.out.println("Счастливый билет");
		} else if ((sumN1N2 / 10) + (sumN1N2 % 10) == (sumN3N4 / 10) + (sumN3N4 % 10)) {
			System.out.println("Счастливый билет");
		} else if ((sumN1N2 / 10) + (sumN1N2 % 10) == sumN3N4) {
			System.out.println("Счастливый билет");
		} else if (sumN1N2 == (sumN3N4 / 10) + (sumN3N4 % 10)) {
			System.out.println("Счастливый билет");
		} else {
			System.out.println("Не счастливый билет");
		}

		// homework 2.8 является ли число палиндромом

		int m1;
		int m2;
		int m3;
		int m4;
		int m5;
		int m6;

		System.out.println("Введите шестизначное число чтобы узнать палиндром или нет");
		happyNumber = sc.nextInt();

		m1 = happyNumber / 100000;
		m2 = happyNumber % 100000 / 10000;
		m3 = happyNumber % 10000 / 1000;
		m4 = happyNumber % 1000 / 100;
		m5 = happyNumber % 100 / 10;
		m6 = happyNumber % 10;

		if (m1 == m6 && m2 == m5 && m3 == m4) {
			System.out.println("Это палиндром");
		} else {
			System.out.println("Это не палиндром");
		}
	}

}
