package com.gmail.ol.bulhakov;

public class Main {

	public static void main(String[] args) {
		// Home work 6.2 Реализация метода
		String startText = "Сумма переменных: ";
		int someNumber = 132;
		double otherNumber = 0.276;
		String text = getText(startText, someNumber, otherNumber);
		System.out.println(text);
	}

	public static String getText(String a, int b, double c) {
		double sum = b + c;
		String text = a + sum;
		return text;

	}

}
