package com.gmail.ol.bulhakov;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// 5.1 Программа для подсчета нечетных цыфр массива

		int[] array = { 0, 5, 2, 4, 7, 1, 3, 19 };
		int numbers = 0;

		for (int i = 0; i < array.length; i += 1) {
			if (array[i] % 2 != 0) {
				numbers += 1;
				System.out.print(array[i]);
				System.out.println("");
			}
		}
		System.out.println("Количество нечетных чисел: " + numbers);

		// 5.2 Создание массива

		int arrayL;
		Scanner sc = new Scanner(System.in);

		System.out.println("Введите длину массива");
		arrayL = sc.nextInt();

		int[] arrayN = new int[arrayL];

		for (int i = 0; i < arrayN.length; i += 1) {
			System.out.println("Введите элемент массива");
			arrayN[i] = sc.nextInt();
		}
		System.out.println(Arrays.toString(arrayN));
		System.out.println("");
		
		// 5.3 Создать массивов

		int[] arrayZ = new int[15];

		for (int i = 0; i < arrayZ.length; i += 1) {

			arrayZ[i] = (int) (Math.random() * 10);

		}
		int[] arrayTwo = Arrays.copyOf(arrayZ, arrayZ.length * 2);

		for (int i = arrayZ.length; i < arrayTwo.length; i += 1) {
			arrayTwo[i] = arrayZ[i - arrayZ.length] * 2;
		}
		System.out.println(Arrays.toString(arrayZ));
		System.out.println("");
		System.out.println(Arrays.toString(arrayTwo));
		System.out.println("");

		// 5.4 Поиск буквы в строке

		

		System.out.println("Input string");
		String text = sc.nextLine();
		
		char[] arrayNew = text.toCharArray();

		int numberOfSymbols = 0;

		for (int i = 0; i < arrayNew.length; i += 1) {
			if (arrayNew[i] == 'b') {
				numberOfSymbols += 1;
			}

		}
		System.out.println(numberOfSymbols);
		
		
	}

}
