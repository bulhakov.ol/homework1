package com.gmail.ol.bulhakov;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// Home work 7.3 из бинарного в десятичный
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Введите число в бинарном формате");
		String bin = sc.nextLine();
		StringBuilder sb = new StringBuilder(bin);
		sb.reverse();
		int dec = 0;
		for (int i = 0; i < sb.length(); i++) {
			if (sb.charAt(i) == '0') {
				dec = (int) (dec + (0 * (Math.pow(2, i))));
			} else if (sb.charAt(i) == '1') {
				dec = (int) (dec + (1 * (Math.pow(2, i))));
			}
		}
		System.out.println("Число в бинарном формате : " + bin);
		System.out.println("Число в десятичном формате : " + dec);

	}

}
