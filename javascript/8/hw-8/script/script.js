const bodyElement = document.querySelector(".container")
const divCreator = () => {
    const someText = document.createElement("div");
    someText.innerHTML = `What I've felt, what I've known<br>
    Never shined through in what I've shown<br>
    Never free, never me<br>
    So I dub thee unforgiven`;
bodyElement.appendChild(someText);
newElements();
}

const newElements = () => {
    let newDivs = bodyElement.childNodes.length
    if (newDivs === 11) {
        while (bodyElement.firstChild) {
            bodyElement.firstChild.remove();
        }
    }
}

