let courses = document.querySelector("#root")
courses.style.backgroundColor = "aqua";
courses.style.fontSize = "20px";
courses.style.border = "2px solid"
courses.style.padding = "5px";
function Valuta(name, sale, buy) {
this.name = name;
this.sale = sale;
this.buy = buy;
this.total = `${this.name} <br> Sale: ${this.sale}; <br>Buy: ${this.buy}.`
};
let usd = new Valuta("USD", 36.96, 38.78);
let euro = new Valuta("EURO", 39.45, 42.65);
let chf = new Valuta("CHF",  40.67, 42.75);
let jpy = new Valuta("JPY", 0.26, 0.29);

let newUsd = document.createElement("p");
newUsd.innerHTML = `${usd.total}`;
courses.appendChild(newUsd);

let newEuro = document.createElement("p");
newEuro.innerHTML = `${euro.total}`;
courses.appendChild(newEuro);

let newChf = document.createElement("P");
newChf.innerHTML = `${chf.total}`;
courses.appendChild(newChf);

let newJpy = document.createElement("p");
newJpy.innerHTML = `${jpy.total}`;
courses.appendChild(newJpy);