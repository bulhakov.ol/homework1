let num1 = "";
let num2 = "";
let sign = "";
let finish = false;

const numbers = ["0","1","2","3","4","5","6","7","8","9","."];
const action =["-","+","X","/","%"];
const screen = document.querySelector(".out");

function clear () {
    num1 = "";
    num2 = "";
    sign = "";
    finish = false;
    screen.textContent = 0;
}
document.querySelector(".ac").addEventListener("click", clear);

document.querySelector(".input").addEventListener("click", function(event){
    if(!event.target.classList.contains("btn")) return;
    if(event.target.classList.contains("ac")) return;
    screen.textContent = "";

    const button = event.target.textContent;

    if(numbers.includes(button)){
        if(num2 === "" && sign === "")
        {num1 += button;
        screen.textContent = num1;
        }
        else if (num1!=="" && num2!=="" && finish){
            num2 = button;
            finish = false;
            screen.textContent = num2;
        }
        else {
            num2 += button;
            screen.textContent = num2;
        }
        console.table(num1, num2, sign);
        return;
    }
    if(action.includes(button)){
        sign = button;
        screen.textContent = sign;
        return;
    }
    if (button === "="){
        if (num2 ==="") num2 = num1;
        switch (sign) {
            case "+":
                    num1 = (+num1) + (+num2);
                    break;
            case "-":
                    num1 = num1 - num2;
                    break;
            case "X":
                    num1 = num1  * num2;
                    break;
            case "/":
                if (num2 === "0") {
                    screen.textContent = "ERROR";
                    num1 = "";
                    num2 = "";
                    sign = "";
                    return;
                }
                    num1 = num1 / num2;
                    break;
            case "%":
                    num1 = num2 / 100 * num1;
                    break;
        }
        finish = true;
        screen.textContent = num1;
    }
  
})
