const btn = document.querySelector(".btn");
let body = document.querySelector("body");
btn.addEventListener("click", () => {
    let d = prompt("Введите диаметр круга максимум 100");
    drawing(d);
})

const drawing = (d) => {
    let template = "";
    for (let i = 0; i < 100; i++) {
        let circleColor = `rgb(${String(Math.floor(Math.random() * 256))},    ${String(Math.floor(Math.random() * 256))},${String(Math.floor(Math.random() * 256))})`;
        let circle = `<div class="circle" style="width: ${d}px; height: ${d}px; background-color: ${circleColor};"></div>`;
        template += circle;
    }
    body.innerHTML = template;
}
body.addEventListener('click', ({target}) => {
    if(target.matches('.circle')) {
        target.style.display = 'none';
    }
})
