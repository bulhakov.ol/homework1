let btn = document.querySelector(".btn");
let body = document.querySelector("body");
btn.addEventListener("click", () => {
    let d = prompt("Введите диаметр круга максимум 100");
    drawing(d);
    btn.style.display = 'none';
    
})

const drawing = (d) => {
    for (let i = 0; i < 100; i++) {
        let circleColor = `rgb(${String(Math.floor(Math.random() * 256))},    ${String(Math.floor(Math.random() * 256))},${String(Math.floor(Math.random() * 256))})`;
        let circle = document.createElement("div");
        circle.classList.add("circle");
        circle.style.height = `${d}px`;
        circle.style.width = `${d}px`;
        circle.style.backgroundColor =`${circleColor}`;
        body.appendChild(circle);
        
    }
    
}
body.addEventListener('click', ({target}) => {
    if(target.matches('.circle')) {
        target.style.display = 'none';
    }
})
