let seconds = 00;
let minuts = 00;
let hours = 00;
let intervalHandler;


const get = id => document.getElementById(id);

const showTime = () => {
    get("output").innerText = `${hours.toString().padStart(2, '0')}:${minuts.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`
};


const count = () => {

    seconds++;
    if (seconds > 59) {
        seconds = 00;
        minuts += 1;
    }

    if (minuts > 59) {
        minuts = 00;
        hours += 1;
    }

    if (hours > 24) {
        hours = 00;
    }
    showTime();
}

get("start_button").onclick = () => {

    intervalHandler = setInterval(count, 1000);
}

get("stop_button").onclick = () => {
    clearInterval(intervalHandler);
}

get("clear_button").onclick = () => {
    seconds = "00";
    minuts = "00";
    hours = "00";
    showTime();
    clearInterval(intervalHandler);
}
get("div").style.backgroundColor = "aqua";
get("div").style.width = "180px";
get("div").style.padding = "2%";
get("div").style.border = "solid"
get("start_button").style.margin = "5px";
get("stop_button").style.margin = "5px";
get("clear_button").style.margin = "5px";
get("output").style.backgroundColor = "yellow";
get("output").style.marginBottom = "5px";
get("output").style.paddingLeft = "57px";


