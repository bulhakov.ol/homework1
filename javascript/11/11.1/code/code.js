let text = document.querySelector(".text");
let btn = document.querySelector(".button");
let saved = false;

btn.addEventListener("click", () => {
    text.value="";
    saved = true;

});

window.addEventListener('beforeunload', (e) => {
    e.preventDefault();
    e.returnValue = "";
  });
  
