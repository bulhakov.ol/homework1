let output = document.querySelector(".output");
window.addEventListener("keydown", (e) => {
    e.preventDefault();
    console.log(e);

    if (e.ctrlKey === true && e.code == "KeyS") {
        output.innerHTML = "!!! Сохранено !!!";
        setTimeout(() => {
            output.innerHTML = "";
        }, 1500)
    }
    if (e.ctrlKey === true && e.code == "KeyA") {
        output.innerHTML = "!!! Выделенно !!!";
        setTimeout(() => {
            output.innerHTML = "";
        }, 1500)
    }
    if (e.ctrlKey === true && e.code == "KeyS" && e.shiftKey === true ) {
        output.innerHTML = "!!! Все сохраненно !!!";
        setTimeout(() => {
            output.innerHTML = "";
        }, 1500)
    }

})