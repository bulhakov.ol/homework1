package com.gmail.ol.bulhakov;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		ProgramLanguage lang1 = new ProgramLanguage("Haskell", DifficultyLevel.HARD);
		ProgramLanguage lang2 = new ProgramLanguage("Python", DifficultyLevel.EASY);
		ProgramLanguage lang3 = new ProgramLanguage("Java", DifficultyLevel.MEDIUM);
		ProgramLanguage lang4 = new ProgramLanguage("C++", DifficultyLevel.HARD);
		ProgramLanguage lang5 = new ProgramLanguage("JS", DifficultyLevel.EASY);

		List<ProgramLanguage> languages = List.of(lang1, lang2, lang3, lang4, lang5);

		System.out.println("Enter level of difficulty EASY or MEDIUM or HARD");
		Scanner sc = new Scanner(System.in);
		String userDifficulty = sc.next();
	

		Stream<ProgramLanguage> stream = languages.stream().filter(a -> a.getDificulty().toString().equals(userDifficulty));
		
		System.out.println(stream.findAny().get());

	}

}
