package com.gmail.ol.bulhakov;

enum DifficultyLevel { EASY, MEDIUM, HARD;
}

public class ProgramLanguage {
	private String name;
	private DifficultyLevel dificulty;
	
	public ProgramLanguage(String name, DifficultyLevel dificulty) {
		super();
		this.name = name;
		this.dificulty = dificulty;
	}

	public ProgramLanguage() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DifficultyLevel getDificulty() {
		return dificulty;
	}

	public void setDificulty(DifficultyLevel dificulty) {
		this.dificulty = dificulty;
	}

	@Override
	public String toString() {
		return "ProgramLanguage [name=" + name + ", dificulty=" + dificulty + "]";
	}
	
	
	
	
	
	
}
