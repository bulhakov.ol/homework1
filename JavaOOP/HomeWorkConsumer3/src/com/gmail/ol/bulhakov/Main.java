package com.gmail.ol.bulhakov;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class Main {

	public static void main(String[] args) {
		File fl = new File ("textFile.txt");
		try {
			fl.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		BiConsumer<String, File> cons = (a,b) ->{
			try(FileWriter fw = new FileWriter(b,true)){
				fw.write(a + " ");
			}catch (IOException e) {
				e.printStackTrace();
			}
		};
		
		String st1 = "I";
		String st2 = "Love";
		String st3 = "Java";
		
		cons.accept(st1,fl);
		cons.accept(st2,fl);
		cons.accept(st3, fl);


	}

}
