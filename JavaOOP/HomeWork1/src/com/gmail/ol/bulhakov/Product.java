package com.gmail.ol.bulhakov;

public class Product {
	private String name;
	private double price;
	private double weith;
	private String information;
	
	
	public Product(String name, double price, double weith, String information) {
		super();
		this.name = name;
		this.price = price;
		this.weith = weith;
		this.information = information;
	}


	public Product() {
		super();
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	public double getWeith() {
		return weith;
	}


	public void setWeith(double weith) {
		this.weith = weith;
	}


	public String getInformation() {
		return information;
	}


	public void setInformation(String information) {
		this.information = information;
	}


	@Override
	public String toString() {
		return "Product [name=" + name + ", price=" + price + ", weith=" + weith + ", information=" + information + "]";
	}
	
	
	
	

}
