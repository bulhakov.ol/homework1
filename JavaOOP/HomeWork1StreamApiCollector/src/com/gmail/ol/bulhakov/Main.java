package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		List<Integer> listInt = new ArrayList<Integer>();
		Random rn = new Random();
		for (int i = 0; i < 100; i += 1) {
			listInt.add(rn.nextInt(100));
		}

		System.out.println("-----------ORIGINAL--LIST-------");
		System.out.println(listInt);
		System.out.println("---------------EVEN-------------");

		String even = listInt.stream().filter(a -> a % 2 == 0).map(a -> a.toString())
				.collect(Collectors.joining("; ", "even: ", "."));

		System.out.println(even);
		System.out.println("---------------ODD--------------");

		String odd = listInt.stream().filter(a -> a % 2 != 0).map(a -> a.toString())
				.collect(Collectors.joining("; ", "odd: ", "."));

		System.out.println(odd);

	}

}
