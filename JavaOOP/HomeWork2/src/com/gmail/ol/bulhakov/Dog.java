package com.gmail.ol.bulhakov;

public class Dog extends Animal{
	private String name;

	public Dog(String ration, String color, int weight, String name) {
		super(ration, color, weight);
		this.name = name;
	}

	public Dog(String ration, String color, int weight) {
		super(ration, color, weight);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String getVoice () {
		String animalVoice = "Гав";
		
		return animalVoice;
	}
	
	@Override
	public void eat() {
		System.out.println("Пес " + name +  " ест продукт - " + super.getRation());
	}
	
	@Override
	public void sleep() {
		System.out.println("Пес " + name + " спит на коврике возле кровати");
	}
	

	@Override
	public String toString() {
		return "Dog [name= " + name +" ]." + " Общая информация "+ super.toString();
	}
}
