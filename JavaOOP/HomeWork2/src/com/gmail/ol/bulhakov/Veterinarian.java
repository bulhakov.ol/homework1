package com.gmail.ol.bulhakov;

public class Veterinarian {
	private String name;

	public Veterinarian(String name) {
		super();
		this.name = name;
	}

	public Veterinarian() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void treatment (Animal animalType) {
		System.out.println(name + " лечит : " + animalType);
	}

	@Override
	public String toString() {
		return "Veterinarian [name= Доктор " + name + "]";
	}
	
	

}
