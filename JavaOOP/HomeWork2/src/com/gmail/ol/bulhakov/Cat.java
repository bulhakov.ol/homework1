package com.gmail.ol.bulhakov;

public class Cat extends Animal{
	private String name;

	public Cat(String ration, String color, int weight, String name) {
		super(ration, color, weight);
		this.name = name;
	}

	public Cat(String ration, String color, int weight) {
		super(ration, color, weight);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String getVoice () {
	    String animalVoice = "Мяу";
		
		return animalVoice;
	}
	
	@Override
	public void eat() {
		System.out.println("Кот " + name + " ест продукт - " + super.getRation());
	}
	
	@Override
	public void sleep() {
		System.out.println("Кот " + name + " спит на батарее");
	}
	

	@Override
	public String toString() {
		return "Cat [name=" + name + "]." + " Общая информация " +super.toString();
	}
	
	

}
