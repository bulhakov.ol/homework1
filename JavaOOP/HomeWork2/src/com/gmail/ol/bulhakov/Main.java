package com.gmail.ol.bulhakov;

public class Main {

	public static void main(String[] args) {
		
		Cat ozzy = new Cat("Рыба", "Черный", 8, "Оззи");
		Dog manson = new Dog("Кость", "Пятнистый", 23, "Мэнсон");
		
		System.out.println(ozzy);
		System.out.println(manson);
		ozzy.eat();
		ozzy.sleep();
		
		String ozzyVoice = ozzy.getVoice();
		System.out.println(ozzyVoice);
		
		manson.eat();
		manson.getVoice();
		
		String mansonVoice = ozzy.getVoice();
		System.out.println(mansonVoice);
		
		
		Veterinarian drDolittle  = new Veterinarian("Дулитл");
		System.out.println(drDolittle);
		
		drDolittle.treatment(manson);
		drDolittle.treatment(ozzy);
		

	}

}
