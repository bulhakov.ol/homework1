package com.gmail.ol.bulhakov;

public class Student {
	private String name;
	private String lastName;
	private int id;
	private char sex;
	
	public Student(String name, String lastName, int id, char sex) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.id = id;
		this.sex = sex;
	}

	public Student() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public char getSex() {
		return sex;
	}

	public void setSex(char sex) {
		this.sex = sex;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", lastName=" + lastName + ", id=" + id + ", sex=" + sex + "]";
	}
	
	
	

}
