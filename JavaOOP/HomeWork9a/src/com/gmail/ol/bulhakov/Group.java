package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class Group {
	private List<Student> groupList;
	private String groupName;

	

	public Group( String groupName) {
		super();
		this.groupList = new ArrayList<Student>();
		this.groupName = groupName;
	}
	
	public Group() {
		super();
		this.groupList = new ArrayList<Student>();
	}
	

	public List<Student> getGroupList() {
		return groupList;
	}

	
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void addStudent (Student student) {
		groupList.add(student);
		
	}
	public void deleteStudentbyLastName (String lastName) {
		for(int i=0; i< groupList.size(); i+=1) {
			if(groupList.get(i).getLastName().equals(lastName)) {
				groupList.remove(i);
				break;
			}
		}System.out.println(System.lineSeparator()+"STUDENT =========>  " + lastName + "  <====== DELETED" + System.lineSeparator());
	}
	public void searchByID (int id) {
		for(int i=0; i< groupList.size();i+=1) {
			if(groupList.get(i).getId() == id) {
				System.out.println(System.lineSeparator()+groupList.get(i)+System.lineSeparator());
				break;
			}
		}
	}
	public void searchByLastName (String lastName) {
		
		for(int i=0; i< groupList.size();i+=1) {
			if(groupList.get(i).getLastName().equals(lastName)) {
				System.out.println(System.lineSeparator()+groupList.get(i)+System.lineSeparator());
				break;
			}
		}
	}

	@Override
	public String toString() {
		String toStringList = groupName + System.lineSeparator();
		for (int i =0; i<groupList.size();i+=1) {
			toStringList += groupList.get(i) + System.lineSeparator();
		}return toStringList;
	}

	
	
	
	
	
	
	
	
}
