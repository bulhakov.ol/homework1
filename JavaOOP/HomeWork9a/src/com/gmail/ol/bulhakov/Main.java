package com.gmail.ol.bulhakov;

public class Main {

	public static void main(String[] args) {
		Student st1 = new Student("Ivan", "Ivanov", 1, 'M');
		Student st2 = new Student("Andrey", "Petrov", 2, 'M');
		Student st3 = new Student("Sergey", "Bobrov", 3, 'M');
		Student st4 = new Student("Aleksandr", "Pugachev", 4, 'M');
		Student st5 = new Student("Irina", "Alegrova", 5, 'W');
		Student st6 = new Student("Ruslana", "Pysanka", 6, 'W');
		Student st7 = new Student("Garry", "Gudiny", 7, 'M');
		Student st8 = new Student("Muslim", "Magomaev", 8, 'M');
		Student st9 = new Student("Lady", "Gaga", 9, 'W');
		
		Group rock777 = new Group();
		rock777.setGroupName("Rock-7-77");
		rock777.addStudent(st1);
		rock777.addStudent(st2);
		rock777.addStudent(st3);
		rock777.addStudent(st4);
		rock777.addStudent(st5);
		rock777.addStudent(st6);
		rock777.addStudent(st7);
		rock777.addStudent(st8);
		rock777.addStudent(st9);
		
		System.out.println(rock777);
		System.out.println("-----------------------------------------");
		System.out.println("-----------------ID--SEARCH--------------");
		rock777.searchByID(5);
		System.out.println("-----------------------------------------");
		System.out.println("-------------LASTNAME--SEARCH------------");
		rock777.searchByLastName("Bobrov");
		System.out.println("-----------------------------------------");
		System.out.println("------------DELETE--BY--LASTNAME---------");
		rock777.deleteStudentbyLastName("Pysanka");
		System.out.println("-----------------------------------------");
		System.out.println("-------------FINAL--GROUP--LIST----------");
		System.out.println(rock777);
	}

}
