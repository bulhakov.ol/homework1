package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		List<String> textList = new ArrayList<>(List.of("Italy", "Ukraine", "Russia", "China", "France", "Poland"));
		textList.removeIf(a -> a.startsWith("F"));
		System.out.println(textList);
	}

}
