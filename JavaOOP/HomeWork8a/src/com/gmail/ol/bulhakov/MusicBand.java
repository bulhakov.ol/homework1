package com.gmail.ol.bulhakov;

public class MusicBand {
	private String groupName;
	private int perswonsInGroup;
	private String styleOfMusic;
	
	public MusicBand(String groupName, int perswonsInGroup, String styleOfMusic) {
		super();
		this.groupName = groupName;
		this.perswonsInGroup = perswonsInGroup;
		this.styleOfMusic = styleOfMusic;
	}

	public MusicBand() {
		super();
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public int getPerswonsInGroup() {
		return perswonsInGroup;
	}

	public void setPerswonsInGroup(int perswonsInGroup) {
		this.perswonsInGroup = perswonsInGroup;
	}

	public String getStyleOfMusic() {
		return styleOfMusic;
	}

	public void setStyleOfMusic(String styleOfMusic) {
		this.styleOfMusic = styleOfMusic;
	}

	@Override
	public String toString() {
		return "MusicBand [groupName=" + groupName + ", perswonsInGroup=" + perswonsInGroup + ", styleOfMusic="
				+ styleOfMusic + "]";
	}
	
	

}
