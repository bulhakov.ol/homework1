package com.gmail.ol.bulhakov;

public class FilmProducing {
	private String filmName;
	private int howManyActors;
	private String filmGener;
	
	public FilmProducing(String filmName, int howManyActors, String filmGener) {
		super();
		this.filmName = filmName;
		this.howManyActors = howManyActors;
		this.filmGener = filmGener;
	}

	public FilmProducing() {
		super();
	}

	public String getFilmName() {
		return filmName;
	}

	public void setFilmName(String filmName) {
		this.filmName = filmName;
	}

	public int getHowManyActors() {
		return howManyActors;
	}

	public void setHowManyActors(int howManyActors) {
		this.howManyActors = howManyActors;
	}

	public String getFilmGener() {
		return filmGener;
	}

	public void setFilmGener(String filmGener) {
		this.filmGener = filmGener;
	}

	@Override
	public String toString() {
		return "FilmProducing [filmName=" + filmName + ", howManyActors=" + howManyActors + ", filmGener=" + filmGener
				+ "]";
	}
	
	
	
}
