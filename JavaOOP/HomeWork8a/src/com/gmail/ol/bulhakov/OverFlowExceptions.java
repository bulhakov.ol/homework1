package com.gmail.ol.bulhakov;

public class OverFlowExceptions extends Exception{

	public OverFlowExceptions() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OverFlowExceptions(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public OverFlowExceptions(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public OverFlowExceptions(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public OverFlowExceptions(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
