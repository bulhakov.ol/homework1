package com.gmail.ol.bulhakov;

import java.util.Arrays;

public class BlackList {
	private final Class [] blackList;

	public BlackList() {
		super();
		blackList = new Class [5];
	}

	public Class[] getBlackList() {
		return blackList;
	}
	public void addInBlackList (Object obj) throws OverFlowExceptions{
		for(int i=0; i<blackList.length; i+=1) {
			if(blackList[i]==null) {
				blackList[i]=obj.getClass();
				return;
			}
		}throw new OverFlowExceptions("ERROR BLACK LIST OVERFLOW");
	}
	

	@Override
	public String toString() {
		return "BlackList [blackList=" + Arrays.toString(blackList) + "]";
	}
	
	
	

	
	
	
	
	
}
