package com.gmail.ol.bulhakov;

import java.util.Arrays;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		String str = "Nirvana was an American rock band formed in Aberdeen, Washington, in 1987. Founded by lead singer and guitarist Kurt Cobain and bassist Krist Novoselic, the band went through a succession of drummers, most notably Chad Channing, before recruiting Dave Grohl in 1990. Nirvana's success popularized alternative rock, and they were often referenced as the figurehead band of Generation X. Their music maintains a popular following and continues to influence modern rock culture.";
		String[] arrStr = str.split(" ");
		Stream<String> stream = Arrays.stream(arrStr).filter(Main::noVowelWordEliminator).sorted(Main::wordComparator);
		stream.forEach(n -> System.out.println(n));

	}

	public static boolean noVowelWordEliminator(String st) {
		String[] arrVowel = new String[] { "A", "E", "U", "I", "O" };
		String[] arrSt = st.split("");
		int vowelCount = 0;
		for (int i = 0; i < arrSt.length; i += 1) {
			for (int j = 0; j < arrVowel.length; j += 1) {
				if (arrSt[i].toUpperCase().equals(arrVowel[j])) {
					vowelCount += 1;
				}
			}
		}
		if (vowelCount > 0) {
			return true;
		}
		return false;
	}

	public static int wordComparator(String str1, String str2) {
		String[] arrVowel = new String[] { "A", "E", "U", "I", "O" };
		String[] arrSt1 = str1.split("");
		String[] arrSt2 = str2.split("");
		int count1 = 0;
		int count2 = 0;

		for (int i = 0; i < arrSt1.length; i += 1) {
			for (int j = 0; j < arrVowel.length; j += 1) {
				if (arrSt1[i].toUpperCase().equals(arrVowel[j])) {
					count1 += 1;
				}
			}
		}
		for (int i = 0; i < arrSt2.length; i += 1) {
			for (int j = 0; j < arrVowel.length; j += 1) {
				if (arrSt2[i].toUpperCase().equals(arrVowel[j])) {
					count2 += 1;
				}
			}
		}
		return count1 - count2;
	}

}
