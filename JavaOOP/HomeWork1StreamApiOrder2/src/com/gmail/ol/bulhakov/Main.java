package com.gmail.ol.bulhakov;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		Student st1 = new Student("Ozzy", "Osbourne", 70);
		Student st2 = new Student("James", "Hetfield", 18);
		Student st3 = new Student("Katy", "Parry", 17);
		Student st4 = new Student("Avril", "Lavin", 37);
		Student st5 = new Student("Billie", "Eilish", 21);
		
		
		
		Student [] arrStudents = new Student [] {st1,st2,st3,st4,st5};
		
		Stream<Student> stream = Arrays.stream(arrStudents)
				.filter(a -> a.getAge()>20)
				.sorted((a,b)->a.getLastName().compareTo(b.getLastName()));
		
		List<Student> stList = stream.collect(Collectors.toList());
		System.out.println(stList);

	}

}
