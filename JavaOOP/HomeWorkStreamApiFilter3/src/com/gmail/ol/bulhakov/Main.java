package com.gmail.ol.bulhakov;

import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		Cat cat1 = new Cat("Ozzy", 1);
		Cat cat2 = new Cat("Mary", 5);
		Cat cat3 = new Cat("Billy", 8);
		Cat cat4 = new Cat("Kitty", 2);
		Cat cat5 = new Cat("Pablo", 10);

		List<Cat> cats = List.of(cat1, cat2, cat3, cat4, cat5);

		List<Cat> catResult = cats.stream().filter(a -> a.getWeight() > 3)
				.sorted((a, b) -> a.getName().compareTo(b.getName())).collect(Collectors.toList());
		System.out.println(catResult);
	}

}
