package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();

		Random rn = new Random();

		for (int i = 0; i < 10; i += 1) {
			list.add(rn.nextInt(10));
		}

		System.out.println(list);
		System.out.println("----------------------------" + System.lineSeparator());

		BinaryOperator<Integer> binOp = (a, b) ->{
			System.out.println("A : " + a + "\n" +"B : " + b);
			return a *b;
		};
		

		Optional<Integer> result = list.stream().filter(a->a>0).reduce(binOp);
		
		System.out.println("----------------------------" + System.lineSeparator());

		System.out.println("Rsult : " + result.get());

	}

}
