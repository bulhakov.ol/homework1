package com.gmail.ol.bulhakov;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class TwoFilesiInOne {
	
	
	public static String fileToString (File file) {
		
		try(BufferedReader bf = new BufferedReader(new FileReader(file))){
			String st = "";
			for(;(st = bf.readLine()) != null;) {
				return st;
				
			}
		}catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static File twoFileInOne (String firstString, String secondString) {
		String [] arrStr1 = firstString.split(" ");
		String [] arrStr2 = secondString.split(" ");
		File file = new File("newFile.txt");
		try {
			file.createNewFile();
		}catch (IOException e) {
			e.printStackTrace();
		}
		try(PrintWriter pw = new PrintWriter(file)){
		for (int i = 0; i < arrStr1.length; i += 1) {
			for (int j = 0; j < arrStr2.length; j +=1) {
				if(arrStr1[i].equals(arrStr2[j])) {
					
						pw.print(arrStr1[i] + " ");
					
				}
			}
		}
		}catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}

}
