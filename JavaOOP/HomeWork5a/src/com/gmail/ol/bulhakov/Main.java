package com.gmail.ol.bulhakov;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		File firstTextFile = new File("/Users/oleksandrbulhakov/Desktop/hw/JavaOOP/HomeWork5a/1.txt");
		File secondTextFile = new File("/Users/oleksandrbulhakov/Desktop/hw/JavaOOP/HomeWork5a/2.txt");
		
		TwoFilesiInOne.twoFileInOne(TwoFilesiInOne.fileToString(firstTextFile), TwoFilesiInOne.fileToString(secondTextFile));

	}

}
