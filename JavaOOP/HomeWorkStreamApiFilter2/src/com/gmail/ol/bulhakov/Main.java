package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		String text = "I Я love Люблю java Джава";
		String [] arr = text.toUpperCase().split("");
		List<String> list = new ArrayList<String>(Arrays.asList(arr));
		
		Stream<String> result = list.stream().filter(Main:: stringPredicate);
		result.forEach(a->System.out.println(a));
		
		
		

	}

	public static boolean stringPredicate(String str) {
		String [] alfabet = new String [] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P",
				"Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
		for (int i = 0; i < alfabet.length; i += 1) {
			if(str.equals(alfabet[i])){
				return true;
			}
			
		}return false;
	}
}
