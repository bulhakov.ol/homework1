package com.gmail.ol.bulhakov;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Group {
	private String groupName;
	private final Student[] students;

	public Group(String groupName) {
		super();
		this.groupName = groupName;
		students = new Student[10];
	}

	public Group() {
		super();
		students = new Student[10];
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Student[] getStudents() {
		return students;
	}

	public void addStudent(Student student) throws GroupOverflowException {
		for (int i = 0; i < students.length; i += 1) {
			if (students[i] == null) {
				students[i] = student;
				return;
			}

		}

		throw new GroupOverflowException("Group owerflow");
	}

	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {

		for (int i = 0; i < students.length; i += 1) {
			if (students[i] != null) {
				if (students[i].getLastName().equals(lastName)) {
					return students[i];
				}
			}
		}
		throw new StudentNotFoundException("Lastname not found");
	}

	public boolean removeStudentById(int id) {

		for (int i = 0; i < students.length; i += 1) {
			if (students[i] != null) {
				if (students[i].getId() == id) {
					students[i] = null;
					return true;
				}

			}
		}
		return false;
	}

	public void sortStudentsByLastName() {
		Arrays.sort(students, Comparator.nullsLast(new StudentLastNameComparator()));
		for (int i = 0; i < students.length; i++) {
			System.out.println(students[i]);
		}
	}
	public void saveListStudents () throws IOException {
		StringBuffer sb = new StringBuffer();
		for (int i = 0 ; i< students.length; i+=1) {
			sb.append(students[i].getSex()+" "+students[i].getName()+ " " + students[i].getLastName() + " \n");
		}String groupListNew = sb.toString();
		
		byte [] arrByte = groupListNew.getBytes();
		File fileGroupList = new File("b.txt");
		try {
			fileGroupList.createNewFile();
		}catch (IOException e) {
			throw e;
		}
		
		try (FileOutputStream fos = new FileOutputStream(fileGroupList)){
			fos.write(arrByte);
		}
		catch (IOException e) {
			throw e;
		}
	} 
	public Group readStudentListFromFile (String nameGroup) throws IOException {
		String text;
	
		try {
		StringBuilder sb = new StringBuilder();
		FileInputStream fis = new FileInputStream("b.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		while ((text = br.readLine()) != null) {
			sb.append(text + System.lineSeparator());
		}
		 text = sb.toString();}
		catch (IOException e) {
			throw e;
		}
		System.out.println(text);
		String [] arrGroupList = text.split(" ");
		
		
		
		Group newGroup = new Group(nameGroup);
		try {
			newGroup.addStudent(new Student(UserSex.valueOf(arrGroupList[0]), arrGroupList[1], arrGroupList[2], 1 , nameGroup));
		} catch (GroupOverflowException e) {
			
			e.printStackTrace();
		}
		
		for(int i=1; i<10; i+=1) {
			
			
			String name = "";
			String lastName ="";
			UserSex gender = null;
			if(arrGroupList[i].equals("Man")) {
				gender= UserSex.Man;
				
			}
			else if (arrGroupList[i].equals("Woman")) {
				gender = UserSex.Woman;
			}
			for (int j =0; j<3; j+=1) {
				
				
				if (j==1 ) {
					name = arrGroupList[j + (i*3)];
				}
				
				
				else if(j==2) {
					lastName = arrGroupList[j + (i*3)];
				}
				
			}
			
			try {
				newGroup.addStudent(new Student(gender, name, lastName, i+1, nameGroup));
			} catch (GroupOverflowException e) {
				e.printStackTrace();;
			}
			
				
		}
		
		
		
		
		
		return newGroup;
		
	}

	@Override
	public String toString() {
		String groupList = groupName + System.lineSeparator();
		for (int i = 0; i < students.length; i += 1) {
			if (students[i] != null) {
				groupList += students[i] + ";" + System.lineSeparator();
			}
		}

		return groupList;
	}

}
