package com.gmail.ol.bulhakov;

public class Student extends Human {
	private int id;
	private String groupName;
	
	
	public Student(UserSex sex, String name, String lastName, int id, String groupName) {
		super(sex, name, lastName);
		this.id = id;
		this.groupName = groupName;
	}


	public Student(UserSex sex, String name, String lastName) {
		super(sex, name, lastName);
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getGroupName() {
		return groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	@Override
	public String toString() {
		return "Student ["+super.toString()+"id=" + id + ", groupName=" + groupName + "]";
	}
	
	
	
	
}
