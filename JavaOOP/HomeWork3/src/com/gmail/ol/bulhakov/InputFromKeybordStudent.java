package com.gmail.ol.bulhakov;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputFromKeybordStudent {
	
public static Student inputStudentFromKeyboard () {
	Scanner sc = new Scanner(System.in);
	

	System.out.println("Add new student: ");
	System.out.print("  Input name: ");
	
	String name = sc.nextLine();
	
	System.out.print("  Input last name: ");
	
	String lastName = sc.nextLine();
	
	System.out.print("  Input gender (Man or Woman): ");
	String newGender = sc.nextLine();
	UserSex gender = UserSex.Man;
	try {
		gender = UserSex.valueOf(newGender);
	} catch (IllegalArgumentException e) {
		e.printStackTrace();
	}

	int id = 0;
	while (id <= 0) {
		try {
			System.out.print("  Input ID number: ");
			id = sc.nextInt();
		} catch (InputMismatchException e) {
			sc.next();
			System.err.println("    Wrong input format. Input integer number!");
		}
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	return new Student(gender, name, lastName, id, "pt-05-1");
}

	
}

