package com.gmail.ol.bulhakov;

import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		Student student1 = new Student(UserSex.Man, "Ozzy", "Osbourne", 1, "pt-05-1");
		Student student2 = new Student(UserSex.Woman, "Amy", "Lee", 2, "pt-05-1");
		Student student3 = new Student(UserSex.Man, "James", "Hetfield", 3, "pt-05-1");
		Student student4 = new Student(UserSex.Man, "Kirk", "Hammett", 4, "pt-05-1");
		Student student5 = new Student(UserSex.Man, "Robert", "Trujillio", 5, "pt-05-1");
		Student student6 = new Student(UserSex.Man, "Lars", "Ulrich", 6, "pt-05-1");
		Student student7 = new Student(UserSex.Woman, "Amy", "Winehouse", 7, "pt-05-1");
		Student student8 = new Student(UserSex.Man, "Axl", "Rose", 8, "pt-05-1");
		Student student9 = new Student(UserSex.Man, "Ville", "Vallo", 9, "pt-05-1");
		Student student10 = new Student(UserSex.Man, "Valeriy", "Kipelov", 10, "pt-05-1");
		Student student11 = new Student(UserSex.Woman, "Tarja", "Turunen", 11, "pt-05-1");

		Group group = new Group("pt-05-1");
		try {
			group.addStudent(student1);
			group.addStudent(student2);
			group.addStudent(student3);
			group.addStudent(student4);
			group.addStudent(student5);
			group.addStudent(student6);
			group.addStudent(student7);
			group.addStudent(student8);
			group.addStudent(student9);
			group.addStudent(student10);

		} catch (GroupOverflowException e) {
			e.printStackTrace();
		}

		

		// поиск студента по фамилии

		Human robert = new Human();
		try {

			robert = group.searchStudentByLastName("Trujillio");
		} catch (StudentNotFoundException e) {
			e.printStackTrace();
		}

	//	System.out.println("Студент которого мы искали" + System.lineSeparator() + robert + System.lineSeparator());

		// Удаление студента по ID

	//	boolean studentIdCancelation = group.removeStudentById(5);
	//	System.out.println("Подтверждение удаления : " + studentIdCancelation + System.lineSeparator());

	//	System.out.println("Список группы после удаления студента по ID" + System.lineSeparator() + group);

		// Переполнение группы

		//try {

		//	group.addStudent(student11);
		//	group.addStudent(student5);

		//} catch (GroupOverflowException e) {
		//	e.printStackTrace();
		//}

		//System.out.println("Список группы после дополнения" + System.lineSeparator() + group);
		
		//Сортировка
	
			System.out.println("Sorted group List");
			group.sortStudentsByLastName();
			System.out.println("-------------------");
		
		//Добавление студента с клавиатуры
		
		//try {
	//		group.addStudent(InputFromKeybordStudent.inputStudentFromKeyboard());

	//	} catch (GroupOverflowException e) {
	//		e.printStackTrace();
	//	}
	//	System.out.println(System.lineSeparator() + group);
		
		try {
			group.saveListStudents();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		
			try {
				Group newGroup = group.readStudentListFromFile("IT-2023");
				System.out.println(newGroup);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			
		
	}

}
