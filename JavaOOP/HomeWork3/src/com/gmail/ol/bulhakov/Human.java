package com.gmail.ol.bulhakov;

public class Human {
	private UserSex sex;
	private String name;
	private String lastName;
	
	
	public Human(UserSex sex, String name, String lastName) {
		super();
		this.sex = sex;
		this.name = name;
		this.lastName = lastName;
	}


	public Human() {
		super();
	}


	public UserSex getSex() {
		return sex;
	}


	public void setSex(UserSex sex) {
		this.sex = sex;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	@Override
	public String toString() {
		return "Human [sex=" + sex + ", name=" + name + ", lastName=" + lastName + "]";
	}
	
	
	
	

}
