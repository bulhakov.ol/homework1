package com.gmail.ol.bulhakov;

import java.util.Comparator;
import java.util.List;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {

		List<Integer> first = List.of(5, 0, 3, 4, 5, 67, 98, 7);
		List<Integer> second = List.of(10, 18, 2, -33, 7, 9);
		Comparator<Integer> cmp = (a, b) -> Integer.compare(a, b);
		BinaryOperator<Integer> binOp = BinaryOperator.minBy(cmp);
		List<Integer> resultList = method(first, second, binOp);
		System.out.println(resultList);
		System.out.println("-----------------------------------");
		
		
		List<String> stList1 = List.of("Kiss","Metallica","Him","Nirvana");
		List<String> stList2 = List.of("Ozzy","Lacrimosa","U2","Ariya");
		Comparator<String> cmpStr = (a,b)-> Integer.compare(a.length(), b.length());
		BinaryOperator<String> binOpSt = BinaryOperator.minBy(cmpStr);
		List<String> strResult = method(stList1, stList2, binOpSt);
		System.out.println(strResult);

	}

	public static <T> List<T> method(List<T> ls1, List<T> ls2, BinaryOperator<T> binOper) {
		boolean firstList = false;
		T first = ls1.get(0);
		for(int i =0; i<ls1.size(); i+=1) {
			first = binOper.apply(first, ls1.get(i));
		}
		T second = ls2.get(0);
		for(int i =0; i< ls2.size(); i+=1) {
			second =binOper.apply(second, ls2.get(i));
		}
		T result = binOper.apply(first, second);
	
		System.out.println(result);
		for(int i =0; i<ls1.size(); i+=1) {
			if(result.equals(ls1.get(i))) {
				firstList=true;
			}
		}
		if(firstList) {
			return ls1;
		}return ls2;

		
	}

}
