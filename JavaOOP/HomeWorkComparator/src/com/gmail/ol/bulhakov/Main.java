package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		Cat cat1 = new Cat("Prometeus", 2);
		Cat cat2 = new Cat("Mik", 6);
		Cat cat3 = new Cat("Thor", 5);
		Cat cat4 = new Cat("Krasty", 4);
		Cat cat5 = new Cat("Elvis", 1);
		Cat cat6 = new Cat("Sisey", 3);

		List<Cat> listCats = new ArrayList<Cat>(List.of(cat1, cat2, cat3, cat4, cat5, cat6));
		System.out.println(listCats);
		System.out.println("------------------------------------------");
		
		Comparator<Cat> comp = Main :: catComparator;
		Collections.sort(listCats, comp);
		System.out.println(listCats);
	}
	
	public static int catComparator (Cat cat1, Cat cat2) {
		if(cat1.getName().length() > cat2.getName().length()) {
			return 1;
		}
		if(cat1.getName().length() < cat2.getName().length()) {
			return -1;
		}return 0;
	}

}
