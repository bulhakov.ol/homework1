package com.gmail.ol.bulhakov;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		Path path = Paths
				.get("/Users/oleksandrbulhakov/Desktop/hw/JavaOOP/HomeWork1StreamApiTerminal/adressCatalog.txt");
		List<String> listCatalogs = new ArrayList<String>();
		try {
			listCatalogs = Files.readAllLines(path);

		} catch (IOException e) {
			e.printStackTrace();
		}

		Stream<String> stream = listCatalogs.stream().filter(Main::stringCatalogPredicate);
		System.out.println(stream.findFirst().get());

	}

	public static boolean stringCatalogPredicate(String str) {
		int countTxtFiles = 0;
		File fl = new File(str);
		if (fl.isDirectory()) {
			String[] flArr = fl.list();
			for (int i = 0; i < flArr.length; i += 1) {

				if (flArr[i].endsWith("txt")) {
					countTxtFiles += 1;
				}
			}

		}
		if (countTxtFiles >= 3) {
			return true;
		}
		return false;

	}

}
