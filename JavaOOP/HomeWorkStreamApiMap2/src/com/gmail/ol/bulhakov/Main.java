package com.gmail.ol.bulhakov;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		Singer sing1 = new Singer("Metallica", new String[] { "Unforgiven", "Master of Puppets", "Fade to black" ,"Unforgiven 2"});
		Singer sing2 = new Singer("Nirvana", new String[] { "Come as you are", "Lithium", "Polly" ,"Rape me"});
		Singer sing3 = new Singer("Anathema", new String[] { "Alone", "Sunset of a age", "Restless oblivion" });

		List<Singer> list = List.of(sing1, sing2, sing3);
		Stream<String> stream = list.stream()
				.flatMap(n->Arrays.stream(n.getSongs()).sorted((o1, o2) -> o1.compareTo(o2)).limit(3));
		stream.forEach(n->System.out.println(n));
			

	}

}
