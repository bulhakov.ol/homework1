package com.gmail.ol.bulhalov;

import java.util.Objects;

public class Cat implements Comparable<Cat>{
	private String name;
	private int length;
	
	
	public Cat(String name, int length) {
		super();
		this.name = name;
		this.length = length;
	}


	public Cat() {
		super();
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getLength() {
		return length;
	}




	public void setLength(int length) {
		this.length = length;
	}


	@Override
	public String toString() {
		return "Cat [name=" + name + ", Length=" + length + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(obj == null) {
			return false;
		}
		if(getClass() != obj.getClass()) {
			return false;
		}
		Cat other = (Cat) obj;
		return length == other.length && Objects.equals(name, other.name);
	}

	@Override
	public int compareTo(Cat o) {
		if (o == null) {
			throw new NullPointerException();
		}
		if(this.length > o.length) {
			return 1;
		}
		if (this.length < o.length) {
			return -1;
		}
		return this.name.compareTo(o.name);
	}


	
	
	
	
	
}
