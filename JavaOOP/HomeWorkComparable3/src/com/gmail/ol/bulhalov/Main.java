package com.gmail.ol.bulhalov;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		Cat cat1 = new Cat("Ozzy", 25);
		Cat cat2 = new Cat("James", 15);
		Cat cat3 = new Cat("Lusy", 13);
		Cat cat4 = new Cat("Black", 30);
		Cat cat5 = new Cat("Micho", 22);
		Cat cat6 = new Cat("Lolly", 16);

		Cat[] arrCat = new Cat[] { cat1, cat2, cat3, cat4, cat5, cat6 };

		Arrays.sort(arrCat);
		for (Cat cat : arrCat) {
			System.out.println(cat);
		}

	}

}
