package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		String text = "I can't remember anything Can't tell if this is true or a dream Deep down inside"
				+" I feel to scream This terrible silence stops me Now that the war is through with me I'm waking up "
				+ "I cannot see That there's not much left of me Nothing is real but pain now";
		
	 List<String> list = new ArrayList<String>(Arrays.asList(text.split(" ")));
	 System.out.println(list);
	 System.out.println("---------------------------------");
	 
	 List<String> strList = list.stream()
			 .filter(a->a.length()<5)
			 .collect(Collectors.toList());
	 System.out.println(strList);

	}

}
