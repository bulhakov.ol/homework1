package com.gmail.ol.bulhakov;

import java.util.function.IntPredicate;

public class NumberAnalizator {
public static boolean numberAnalizator (int number) {
	IntPredicate pr = (a) -> a%2 ==0;
	String numbrString = Integer.toString(number);
	char [] arr = numbrString.toCharArray();
	int sum = 0;
	for (int i =0 ; i< arr.length; i+=1) {
		sum += Character.getNumericValue(arr[i]);
	}
	System.out.println("Sum of all number elements ---->  "+sum);
	System.out.println("--------------------------------------");
	return pr.test(sum);
}
}
