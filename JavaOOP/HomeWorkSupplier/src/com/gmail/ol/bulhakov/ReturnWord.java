package com.gmail.ol.bulhakov;

import java.util.Random;
import java.util.function.Supplier;

public class ReturnWord implements Supplier<String>{
	private String someText;

	public ReturnWord(String someText) {
		super();
		this.someText = someText;
	}

	public ReturnWord() {
		super();
	}

	public String getSomeText() {
		return someText;
	}

	public void setSomeText(String someText) {
		this.someText = someText;
	}

	@Override
	public String toString() {
		return "ReturnWord [someText=" + someText + "]";
	}

	@Override
	public String get() {
		String [] arrStr = someText.split(" ");
		Random rn = new Random();
		return arrStr[rn.nextInt(arrStr.length)];
	}
	
	
}
