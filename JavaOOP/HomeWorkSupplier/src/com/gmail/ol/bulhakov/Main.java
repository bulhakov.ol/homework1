package com.gmail.ol.bulhakov;

import java.util.function.Supplier;

public class Main {

	public static void main(String[] args) {
		Supplier<String> sup = new ReturnWord("I love Java Programming Java The Best");
		System.out.println(sup.get());
		System.out.println("----------------------------------");
		System.out.println(sup.get());
		System.out.println("----------------------------------");
		System.out.println(sup.get());
		System.out.println("----------------------------------");

	}

}
