package com.gmail.ol.bulhakov;

public class Port {
	private volatile int loadShips;
	private int containers;
	
	public int getLoadShips() {
		return loadShips;
	}
	public void setLoadShips(int loadShips) {
		this.loadShips = loadShips;
	}
	public synchronized void portLoading (int boatContainers, Port port, String boatName ) {
		this.setLoadShips(getLoadShips() + 1);
		if (loadShips > 2) {
			try {
				wait();
			}catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		for(int i = 1; i<=boatContainers; i+=1 ) {
			try {
				wait(500);
			}catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Корабль: "+boatName+ ";  контейнер номер: " + i);
			containers +=1;
		}
		System.out.println("total numbers of containers in port" +containers);
		
		notifyAll();
		this.setLoadShips(getLoadShips() -1);
	}
	
	
}
