package com.gmail.ol.bulhakov;

public class Boat implements Runnable{
	private int containers;
	private String boatName;
	private Port port;
	
	public Boat(int containers, String boatName, Port port) {
		super();
		this.containers = containers;
		this.boatName = boatName;
		this.port = port;
	}

	public Boat() {
		super();
	}

	public int getContainers() {
		return containers;
	}

	public void setContainers(int containers) {
		this.containers = containers;
	}

	public String getBoatName() {
		return boatName;
	}

	public void setBoatName(String boatName) {
		this.boatName = boatName;
	}

	public Port getPort() {
		return port;
	}

	public void setPort(Port port) {
		this.port = port;
	}
	

	@Override
	public void run() {
		port.portLoading(containers, port, boatName);
		this.setContainers(0);
		
	}

	@Override
	public String toString() {
		return "Boat [containers=" + containers + ", boatName=" + boatName + ", port=" + port + "]";
	}
	
	
	
	
	

}
