package com.gmail.ol.bulhakov;

public class Main {

	public static void main(String[] args) {
		Port mainPort = new Port();
		
		Boat firstBoat = new Boat(10, "First Boat", mainPort);
		Boat secondBoat = new Boat(10, "Second Boat", mainPort);
		Boat thirdBoat = new Boat(10, "Third Boat", mainPort);
		
		Thread th1 = new Thread(firstBoat);
		Thread th2 = new Thread(secondBoat);
		Thread th3 = new Thread (thirdBoat);
		
		th1.start();
		th2.start();
		th3.start();
		try {
			th1.join();
			th2.join();
			th3.join();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("All boats loaded containers");
	
		

	}

}
