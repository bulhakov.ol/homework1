package com.gmail.ol.bulhakov;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class WebAdressController {

	private File adressFile;

	public WebAdressController(File adressFile) {
		super();
		this.adressFile = adressFile;
	}

	public WebAdressController() {
		super();
	}

	public File getAdressFile() {
		return adressFile;
	}

	public void setAdressFile(File adressFile) {
		this.adressFile = adressFile;
	}
	
	public Map<String, String> webAdressControl () throws  IOException{
		Path path = adressFile.toPath();
		List<String> adressList = Files.readAllLines(path);
		System.out.println(adressList);
		Map<String, String> result = new HashMap<String, String>();
		for(int i = 0; i<adressList.size(); i+=1) {
			URL url = new URL(adressList.get(i));
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			int responseCode = con.getResponseCode();
			if(200 == responseCode) {
				result.put(adressList.get(i), "Response Code from 200 to 300");
			}else {
				result.put(adressList.get(i), "Connection have some problem");
			}
			
		}return result;
	}

	@Override
	public String toString() {
		return "WebAdressController [adressFile=" + adressFile + "]";
	}
	
	
}
