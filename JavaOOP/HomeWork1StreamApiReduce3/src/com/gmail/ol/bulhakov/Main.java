package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		Random rn = new Random();
		
		for(int i =0; i< 150; i+=1) {
			list.add(rn.nextInt(1000));
		}
		
		System.out.println(list);
		System.out.println("-----------MAX--METHOD-----------");

		Optional<Integer> maxMetodResult = list.stream()
				.max((a,b)->a-b);
		
		System.out.println(maxMetodResult.get());
		
		System.out.println("---------REDUCE--METHOD-----------");
		
		BinaryOperator<Integer> binOp = (a,b) -> {
			if(a>b) {
				return a;
			}
			return b;
		};
		
		Optional<Integer> reduceMethodResult = list.stream()
				.reduce(binOp);
		
		System.out.println(reduceMethodResult.get());
		
	}

}
