package com.gmail.ol.bulhakov;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {

		Path path = Paths
				.get("/Users/oleksandrbulhakov/Desktop/hw/JavaOOP/HomeWork1StreamApiCollector2/CatalogForTest");
		List<File> listFl = null;
		try {
			listFl = Files.walk(path).map(a -> a.toFile()).collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		listFl.remove(0);
		System.out.println(listFl);
		System.out.println("------------------------------" + System.lineSeparator());

		Function<File, String> keyKreator = a -> {
			if (a.isFile()) {
				if (a.length() > (1024 * 100)) {
					return "More then 100kb";
				}
				return "Less then 100kb";
			}
			return "Is Directory";
		};

		Map<String, List<File>> resultMap = listFl.stream().collect(Collectors.groupingBy(keyKreator));

		resultMap.forEach((k, v) -> System.out.println(k + "\t" + v));

	}

}
