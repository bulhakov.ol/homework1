package com.gmail.ol.bulhakov;
import java.util.HashMap;
import java.util.Map;

public class TextTransformator {

	public void textToASCIIString(String text) {
		Map<Character, String[]> map = new HashMap<>();

		map.put('a', new String[] { "  *   ", " * *  ", "*   * ", "*****", "*   * ", "*   * " });
		map.put('b', new String[] { "****  ", "*   * ", "****  ", "*   * ", "*   * ", "****  " });
		map.put('c', new String[] { "   *   ", "  *    ", " *     ", " *     ", "  *    ", "   *   " });
		map.put('d', new String[] { "****   ", "*   *  ", "*   *  ", "*   *  ", "*  **  ", "***  * " });
		map.put('e', new String[] { "***** ", "*     ", "*     ", "****  ", "*     ", "***** " });
		map.put('f', new String[] { "***** ", "*     ", "*     ", "****  ", "*     ", "*     " });
		map.put('g', new String[] { "   *** ", "  *   *", " *     *", "  *****", "      *", "  ***  *" });
		map.put('h', new String[] { "*   * ", "*   * ", "*   * ", "***** ", "*   * ", "*   * " });
		map.put('i', new String[] { "*****", "  *  ", "  *  ", "  *  ", "  *  ", "*****" });
		map.put('j', new String[] { "*****", "   *  ", "   *  ", "   *  ", "*  *  ", "**    " });
		map.put('k', new String[] { "*   * ", "*  *  ", "* *   ", "**    ", "* *   ", "*  *  " });
		map.put('l', new String[] { "*     ", "*     ", "*     ", "*     ", "*     ", "***** " });
		map.put('m', new String[] { "*   * ", "** ** ", "* * * ", "*   * ", "*   * ", "*   * " });
		map.put('n', new String[] { "*   * ", "**  * ", "* * * ", "*  ** ", "*   * ", "*   * " });
		map.put('o', new String[] { " ***  ", "*   * ", "*   * ", "*   * ", "*   * ", " ***  " });
		map.put('p', new String[] { "****  ", "*   * ", "*   * ", "****  ", "*     ", "*     " });
		map.put('q', new String[] { " ***  ", "*   * ", "*   * ", "* * * ", " *   *", "  *** " });
		map.put('r', new String[] { "****  ", "*   * ", "*   * ", "****  ", "*  *  ", "*   * " });
		map.put('s', new String[] { " **** ", "*     ", "*     ", " ***  ", "     *", "****  " });
		map.put('t', new String[] { "*****", "  *  ", "  *  ", "  *  ", "  *  ", "  *  " });
		map.put('u', new String[] { "*   *", "*   *", "*   *", "*   *", "*   *", " *** " });
		map.put('v', new String[] { "*   *", "*   *", "*   *", " * * ", "  *  ", "  *  " });
		map.put('w', new String[] { "*   *", "*   *", "*   *", "* * *", "** **", "*   *" });
		map.put('x', new String[] { "*   *", " * * ", "  *  ", " * * ", "*   *", "*   *" });
		map.put('y', new String[] { "*   *", " * * ", "  *  ", "  *  ", "  *  ", "  *  " });
		map.put('z', new String[] { "*****", "   * ", "  *  ", " *   ", "*    ", "*****" });

		for (int i = 0; i < text.length(); i++) {
			char cr = Character.toLowerCase(text.charAt(i));
			String[] asciiArt = map.get(cr);
			if (asciiArt != null) {
				for (String s : asciiArt) {
					System.out.println(s);
					

				}
			} else {
				System.out.println(cr);

			}
		}

	}
}
