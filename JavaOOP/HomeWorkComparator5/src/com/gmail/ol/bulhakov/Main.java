package com.gmail.ol.bulhakov;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		File fl1 = new File("/Users/oleksandrbulhakov/Desktop/hw/JavaOOP/HomeWorkComparator5/file1.txt");
		File fl2 = new File("/Users/oleksandrbulhakov/Desktop/hw/JavaOOP/HomeWorkComparator5/file2.txt");
		File fl3 = new File("/Users/oleksandrbulhakov/Desktop/hw/JavaOOP/HomeWorkComparator5/file3.txt");
	
		List<File> flList = new ArrayList<File>(List.of(fl1,fl2,fl3));
		System.out.println(flList);
		System.out.println("---------------------------");
		
		Comparator<File> comp = Main :: fileComparator;
		Collections.sort(flList, comp);
		System.out.println(flList);
		
		
		
	}
	
	
	
	public static int fileComparator (File file1, File file2) {
		String str1 = fileToString(file1);
		String str2 = fileToString(file2);
		if(signCounter(str1) > signCounter(str2)) {
			return 1;
		}
		if(signCounter(str1)<signCounter(str2)) {
			return -1;
		}return 0;
		
	}
	
	
	public static String fileToString (File file) {
		StringBuilder sb = new StringBuilder();
		String text ="";
		try(BufferedReader br = new BufferedReader(new FileReader(file))){
			for(;(text = br.readLine())!=null;) {
				sb.append(text);
				sb.append(System.lineSeparator());
			}
		}catch (IOException e) {
			e.printStackTrace();
		}return sb.toString();
		
	}
	
	public static int signCounter (String str) {
		char [] arr = new char [] {',','.','!','?'};
		char [] strArr = str.toCharArray();
		int countSign = 0;
		
		for( int i = 0; i < strArr.length; i+=1) {
			for(int j =0; j< arr.length; j+=1) {
				if (strArr[i] == arr[j]) {
					countSign +=1;
				}
			}
		}return countSign;
		
	}

}
