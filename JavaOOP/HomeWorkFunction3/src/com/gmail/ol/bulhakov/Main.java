package com.gmail.ol.bulhakov;

import java.util.function.Function;

public class Main {

	public static void main(String[] args) {
		String text = "She was barely dressed though, And the great indiscreet trees Touched the glass with their leaves,In malice, quite close, quite close.";

		Function<String, Integer> func = (a) ->{
			int sum = 0;
			char [] arr = a.toCharArray();
			for ( int i =0; i< arr.length; i+=1) {
				sum += arr[i];
			}return sum;
			
		};
		System.out.println(func.apply(text));
	}

}
