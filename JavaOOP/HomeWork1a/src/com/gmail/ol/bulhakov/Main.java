package com.gmail.ol.bulhakov;

public class Main {

	public static void main(String[] args) {
		Triangle triangleOne = new Triangle(5, 6, 7);
		Triangle triangleTwo = new Triangle(10, 15, 20);
		Triangle triangleThree = new Triangle(11, 33, 34);
		
		double firstTriangleArea = triangleOne.getTriangleArea();
		double secondTriangleArea = triangleTwo.getTriangleArea();
		double thirdTriangleArea = triangleThree.getTriangleArea();
		
		System.out.println("Площадь первого треугольника : " + firstTriangleArea);
		System.out.println("Площадь второго треугольника : " + secondTriangleArea);
		System.out.println("Площадь третьего треугольника : " + thirdTriangleArea);

		
	}

}
