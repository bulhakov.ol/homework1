package com.gmail.ol.bulhakov;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyFileThread implements Runnable{
	private File [] fileForCopy;
	private File folder;
	int start;
	int end;
	
	
	

	

	public CopyFileThread(File[] fileForCopy, File folder, int start, int end) {
		super();
		this.fileForCopy = fileForCopy;
		this.folder = folder;
		this.start = start;
		this.end = end;
	}
	private  void copyFiles () throws IOException{
		for(int i=start; i<end; i+=1) {
			if(fileForCopy[i].isFile()) {
				
				File newFile = new File(folder, fileForCopy[i].getName());
				newFile.createNewFile();
				
				
		byte [] buffer = new byte [1024 * 1024];
		int readByte = 0;
		try (FileInputStream fis = new FileInputStream(fileForCopy[i]); FileOutputStream fos = new FileOutputStream(newFile)){
			for(;(readByte = fis.read(buffer))>0;) {
				fos.write(buffer,0,readByte);
			}
		}catch (IOException e) {
			throw e;
		}
		}
		}
	}
	@Override
	public void run () {
		Thread th = Thread.currentThread();
		try {
			copyFiles();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(th.getName());
	}
	
	
}
