package com.gmail.ol.bulhakov;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		File folder = new File("/Users/oleksandrbulhakov/Desktop/hw/JavaOOP/HomeWork6b/folderIn");
		File folderOut = new File ("/Users/oleksandrbulhakov/Desktop/hw/JavaOOP/HomeWork6b/folderOut");
		File [] fileArr = folder.listFiles();
		
		Thread thOne = new Thread(new CopyFileThread(fileArr, folderOut, 0, fileArr.length/4));
		Thread thTwo = new Thread(new CopyFileThread(fileArr, folderOut, fileArr.length/4, fileArr.length/4*2));
		Thread thThree = new Thread(new CopyFileThread(fileArr, folderOut, fileArr.length/4*2, fileArr.length/4*3));
		Thread thFour = new Thread(new CopyFileThread(fileArr, folderOut, fileArr.length/4*3, fileArr.length));
		
		
		thOne.start();
		thTwo.start();
		thThree.start();
		thFour.start();
		
		try {
			thOne.join();
			thTwo.join();
			thThree.join();
			thFour.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("The process is complete");
		
	}

}
