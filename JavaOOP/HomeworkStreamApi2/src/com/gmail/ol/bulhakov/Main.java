package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<Integer>();
		Random rn = new Random();
		for (int i = 0; i < 100; i += 1) {
			numbers.add(rn.nextInt(100));
		}
		System.out.println(numbers);
		System.out.println("-----------------------------");

		Stream<Integer> intStream = numbers.stream().filter(a -> a % 2 != 0).sorted();
		List<Integer> result = intStream.collect(Collectors.toList());
		System.out.println(result);

	}

}
