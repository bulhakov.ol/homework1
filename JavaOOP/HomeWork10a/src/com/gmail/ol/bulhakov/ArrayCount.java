package com.gmail.ol.bulhakov;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ArrayCount {

	
	
	
	public void arrayCounter (int arrSize){
		int [] arrInteger= new int [arrSize];
		Random rnd = new Random();
		for(int i=0; i< arrInteger.length; i+=1) {
			arrInteger[i]=rnd.nextInt(0,9);
		}
		System.out.println("----------------ARRAY----------------");
		System.out.println(Arrays.toString(arrInteger));
		Map<Integer, Integer> countMap = new HashMap<Integer, Integer>();
		for(int i =0; i< arrInteger.length; i+=1) {
			if(countMap.get(arrInteger[i]) == null) {
				countMap.put(arrInteger[i], 1);
			}else {
				countMap.put(arrInteger[i], countMap.get(arrInteger[i]) + 1);
			}
		}
		System.out.println("-----------COUNT--RESULT--------------");
		StringBuilder sb = new StringBuilder();
		countMap.forEach((number,count) -> sb.append(number + "  =>  " + count + System.lineSeparator()));
		System.out.println(sb.toString());
	}
}
