package com.gmail.ol.bulhakov;

import java.io.BufferedReader;
import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class FileEditor {
	private final static char [] arr = new char [] {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S',
			'T','U','V','W','X','Y','Z'};
	private final int [] arrInt= new int [26];

	
	
	
	


	public String FileToString (File file)  {
		
		StringBuilder sb = new StringBuilder();
		try(BufferedReader br = new BufferedReader(new FileReader(file))){
			String text;
			for (;(text = br.readLine())!=null;) {
				sb.append(text.toUpperCase()).append(System.lineSeparator());
			}
		}catch (IOException e) {
			e.printStackTrace();
		}return sb.toString();
 }
	

	
	public void stringToCharArr (String string) {
		char [] charArr = string.toCharArray();
		for (int i =0; i< charArr.length;i+=1) {
			for(int j =0; j<arr.length;j +=1) {
				if(arr[j]==charArr[i]){
					arrInt[j]+=1;
				}
			}
		}
	}
	
	public void listOfRepetLetteres (){
		List<Letter> letterList =new ArrayList<Letter>();
		for(int i =0; i< arrInt.length; i+=1) {
			letterList.add(new Letter(arr[i], arrInt[i]));
		}
		System.out.println(letterList);
		System.out.println("------------------------------------");
		System.out.println("------------SORTED--LIST------------");
		Collections.sort(letterList,new LetterCountComparator()); 
		System.out.println(letterList);
		System.out.println("------------------------------------");
		
		
	}
	
}