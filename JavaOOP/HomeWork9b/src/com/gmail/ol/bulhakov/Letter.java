package com.gmail.ol.bulhakov;

public class Letter {
	private char letterName;
	private int letterCount;
	
	public Letter(char letterName, int letterCount) {
		super();
		this.letterName = letterName;
		this.letterCount = letterCount;
	}

	public Letter() {
		super();
	}

	public char getLetterName() {
		return letterName;
	}

	public void setLetterName(char letterName) {
		this.letterName = letterName;
	}

	public int getLetterCount() {
		return letterCount;
	}

	public void setLetterCount(int letterCount) {
		this.letterCount = letterCount;
	}

	@Override
	public String toString() {
	
		return System.lineSeparator()+"letter= " + letterName + ", Count= " + letterCount; 
	}
	
	
	

}
