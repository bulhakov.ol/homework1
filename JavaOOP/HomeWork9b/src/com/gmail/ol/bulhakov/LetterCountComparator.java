package com.gmail.ol.bulhakov;

import java.util.Comparator;

public class LetterCountComparator implements Comparator<Letter>{

	@Override
	public int compare(Letter o1, Letter o2) {
		return o2.getLetterCount()-o1.getLetterCount();
	}
	
}
