package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Main {

	public static void main(String[] args) {
		Predicate<String> pr = a -> Character.isUpperCase(Character.valueOf(a.charAt(0)));
		
		List<String> listStr = List.of("Yesterday","dog","Smile","cat","forest","Bird");
		List<String> result = new ArrayList<String>();
		
		for(int i =0; i< listStr.size(); i +=1) {
			
			int index =i;
			Supplier<String> sp = ()-> pr.test(listStr.get(index)) == true ? listStr.get(index) : null;
			result.add(sp.get());
		}
		Collections.sort(result, Comparator.nullsLast(Comparator.naturalOrder()));
		System.out.println(result);

	}

}
