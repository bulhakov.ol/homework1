package com.gmail.ol.bulhakov;

import java.util.ArrayList;

import java.util.List;
import java.util.function.Predicate;

public class SimpleNumberCoun {
	public static int simpleNumberCounter(Integer[] arr) {
		List<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < arr.length; i += 1) {
			list.add(arr[i]);
		}

		Predicate<Integer> pr = a -> {

			for (int i = 2; i < a; i += 1) {
				if (a % i == 0) {
					return true;
				}
			}
			return false;
		};
		list.removeIf(pr);

		int count = 0;
		for (int i = 0; i < list.size(); i += 1) {
			count += 1;
		}

		return count;
	}
}
