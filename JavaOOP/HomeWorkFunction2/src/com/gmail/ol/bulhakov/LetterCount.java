package com.gmail.ol.bulhakov;

public class LetterCount {

	
	public static int letterCount (String str) {
		int letterCount = 0;
		char [] letters = new char [] {'B','C','D','F','G','H','J','K','L','M','N','P','R','S','T','V',
				'W','X','Z'};
		char [] stringArray = str.toUpperCase().toCharArray();

		for(int i = 0; i< stringArray.length; i +=1) {
			for (int j = 0; j < letters.length; j+=1) {
				if( stringArray[i] == letters[j]) {
					letterCount += 1;
				}
			}
		}return letterCount;
		
	}
}
