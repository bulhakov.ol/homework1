package com.gmail.ol.bulhakov;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class Client implements Runnable{

	private Socket socket;
	private String text;
	private Thread thread;
	
	public Client(Socket socket, String text) {
		super();
		this.socket = socket;
		this.text = text;
		thread = new Thread();
		thread.start();
	}

	@Override
	public void run() {
		try (InputStream is = socket.getInputStream(); OutputStream os = socket.getOutputStream() ){
			PrintWriter pw = new PrintWriter(os);
			byte [] bt = new byte [is.available()];
			is.read(bt);
			String responce ="HTTP/1.1 200 OK\r\n" + "Server: My_Server\r\n" + "Content-Type: text/html\r\n+ " +"Content-Length: " + "\r\n" + "Connection: close\r\n\r\n";
			pw.print(responce + text);
			pw.flush();
		}catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	

}
