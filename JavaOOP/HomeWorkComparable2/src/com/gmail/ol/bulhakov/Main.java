package com.gmail.ol.bulhakov;

import java.util.Arrays;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		Integer [] arr = new  Integer [100] ;
		Random rm = new Random();
		for (int i = 0; i < arr.length; i+=1) {
			arr[i]= rm.nextInt(55555);
		}
		System.out.println(Arrays.toString(arr));
		System.out.println("--------------------------------------");
		System.out.println(maximal(arr));
	}
	public static <T extends Comparable<T>> T maximal (T[] arr) {
		T maximal = arr[0];
		for (int i =0; i<arr.length; i +=1) {
			if(arr[i].compareTo(maximal)>0) {
				maximal = arr[i];
			}
		}return maximal;
	}

}
