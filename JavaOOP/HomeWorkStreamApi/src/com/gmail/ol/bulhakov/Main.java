package com.gmail.ol.bulhakov;

import java.util.List;
import java.util.Optional;


public class Main {

	public static void main(String[] args) {
		Cat cat1 = new Cat("Ozzy");
		Cat cat2 = new Cat("Billy");
		Cat cat3 = new Cat("Mozart");
		Cat cat4 = new Cat("Kitty");
		Cat cat5 = new Cat("Bob");

		List<Cat> listCat = List.of(cat1, cat2, cat3, cat4, cat5);
		
		Optional <String> catStream = listCat.stream()
				.max((a,b) -> a.getName().length() -b.getName().length()).map(a -> a.getName());
		String catName = catStream.get();
		System.out.println(catName);
		

	}

}
