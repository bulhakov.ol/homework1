package com.gmail.ol.bulhakov;

public class Cat {
	private String name;

	public Cat(String name) {
		super();
		this.name = name;
	}

	public Cat() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Cat [name=" + name + "]";
	}

}
