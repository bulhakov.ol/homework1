package com.gmail.ol.bulhakov;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.DirectoryStream.Filter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LinkSearch {
	private String link;

	public LinkSearch(String link) {
		super();
		this.link = link;
	}

	public LinkSearch() {
		super();
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String linkSearch() {
		StringBuilder sb = new StringBuilder();
		try {
			URL url = new URL(link);
			HttpURLConnection hUC = (HttpURLConnection) url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(hUC.getInputStream()));
			String text = "";
			for (; (text = br.readLine()) != null;) {
				sb.append(text).append(System.lineSeparator());
			}

		} catch (IOException e) {
			System.out.println("NO URL CONNECTION");
		}
		return sb.toString();
	}

	public List<String> returnLinks() {
		String [] arrText = linkSearch().split(" ");
		List<String> list = List.of(arrText);
		String linkTeg = "href=\"http";
		
		Stream<String> resultList = list.stream()
				.filter(a->a.startsWith(linkTeg) && a.endsWith("\""))
				.map(a->a.substring(6, a.length()-1));
		
		List<String> result = resultList.collect(Collectors.toList());
		return result;
		
	}

}
