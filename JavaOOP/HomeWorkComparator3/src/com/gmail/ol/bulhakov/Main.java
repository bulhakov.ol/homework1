package com.gmail.ol.bulhakov;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		Integer [] arr = new Integer [25];
		Random rn = new Random();
		
		for (int i =0; i< arr.length; i+=1) {
			arr[i]= rn.nextInt(24567);
		}
		System.out.println(Arrays.toString(arr));
		System.out.println("--------------------------------------");
		
		Comparator<Integer> compInt = Main :: numberComparator;
		Arrays.sort(arr, compInt);
		System.out.println(Arrays.toString(arr));

	}
	
	
	public static int numberComparator (Integer int1, Integer int2) {
		int int1Length = (int)(Math.log10(int1)+1);
		int int2Length = (int)(Math.log10(int2)+1);
	
		int sum1 = (int)(int1/(Math.pow(10, int1Length-1))) + (int1%10);
		int sum2 = (int)(int2/(Math.pow(10, int2Length-1))) + (int2%10);
		
		if(sum1 > sum2) {
			return 1;
		}
		if(sum1 < sum2) {
			return-1;
		}
		return 0;
		
		
	}

}
