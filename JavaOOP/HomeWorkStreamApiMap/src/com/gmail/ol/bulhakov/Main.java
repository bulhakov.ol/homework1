package com.gmail.ol.bulhakov;


import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;



public class Main {

	public static void main(String[] args) {
		
		try {
			Stream<String> flStream = Files.lines(Path.of("/Users/oleksandrbulhakov/Desktop/hw/JavaOOP/HomeWorkStreamApiMap/1.txt")).filter(url->isActive(url));
			flStream.forEach(st->System.out.println(st));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static boolean isActive (String st) {
		try {
		HttpURLConnection connection = (HttpURLConnection) new URL(st).openConnection();
		connection.setRequestMethod("GET");
		int result = connection.getResponseCode();
		if(result == 200) {
			return true;
		}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}return false;
	}

}
