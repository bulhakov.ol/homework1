package com.gmail.ol.bulhakov;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.function.ToIntFunction;



public class Main {

	public static void main(String[] args) {
		Calendar cl = Calendar.getInstance();
		Date date = cl.getTime();
		ToIntFunction<Date> func = Main :: dateYearToInt;
		System.out.println(func.applyAsInt(date));
	


	}
	
	public static int dateYearToInt (Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY");
		String year = sdf.format(date);
		int intYear = Integer.parseInt(year);
		return intYear;
	}

}
