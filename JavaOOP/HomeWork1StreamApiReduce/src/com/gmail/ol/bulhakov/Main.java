package com.gmail.ol.bulhakov;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {
		String someText = "The judge in the New York case has already decided Donald Trump inflated his finances. Now, he will make rulings on Mr. Trump’s future as a businessman.";
		String[] arrText = someText.split(" ");

		BiFunction<Integer, String, Integer> biFun = (a, b) -> a + b.length();
		BinaryOperator<Integer> binOp = (a, b) -> a + b;

		Integer sum = Arrays.stream(arrText).filter(a -> a.length() > 4).reduce(0, biFun, binOp);

		System.out.println(sum);

	}

}
