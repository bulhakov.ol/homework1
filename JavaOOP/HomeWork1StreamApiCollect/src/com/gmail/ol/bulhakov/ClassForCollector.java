package com.gmail.ol.bulhakov;

import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class ClassForCollector implements Collector<String, Set<String>, Set<String>> {
	private Predicate<String> pr;
	
	

	public ClassForCollector(Predicate<String> pr) {
		super();
		this.pr = pr;
	}
	

	public ClassForCollector() {
		super();
	}
	
	


	public Predicate<String> getPr() {
		return pr;
	}


	public void setPr(Predicate<String> pr) {
		this.pr = pr;
	}


	@Override
	public Supplier<Set<String>> supplier() {
		return HashSet :: new;
	}

	@Override
	public BiConsumer<Set<String>, String> accumulator() {
		return(a,b) -> {
			if(pr.test(b)) {
				a.add(b);
			}
		};
	}

	@Override
	public BinaryOperator<Set<String>> combiner() {
		return (a,b)->{
		Set <String> resultSet = new HashSet<String>();
		resultSet.addAll(a);
		resultSet.addAll(b);
		return resultSet;
		};
	}

	@Override
	public Function<Set<String>, Set<String>> finisher() {
		
		return Function.identity();
	}

	@Override
	public Set<Characteristics> characteristics() {
		// TODO Auto-generated method stub
		return Set.of(Characteristics.IDENTITY_FINISH);
	}

		
}
