package com.gmail.ol.bulhakov;

import java.util.Arrays;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collector;

public class Main {

	public static void main(String[] args) {
		String text ="Former President Trump is accused in a civil lawsuit of inflating his net worth to win favorable terms on loans. He has said he is being persecuted.";
		
		Predicate<String> predic = a-> a.length()>3;
		Collector<String, Set<String>, Set<String>> myCollector = new ClassForCollector(predic);
		
		Set<String> resultSet = Arrays.stream(text.split(" ")).collect(myCollector);
		
		System.out.println(resultSet);
		
		

	}

}
