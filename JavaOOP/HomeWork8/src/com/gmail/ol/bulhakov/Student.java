package com.gmail.ol.bulhakov;

import java.io.Serializable;

public class Student implements Serializable{
	private String name;
	private String lastName;
	private int age;
	private int id;
	private char sex;
	
	public Student(String name, String lastName, int age, int id, char sex) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.age = age;
		this.id = id;
		this.sex = sex;
	}

	public Student() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public char getSex() {
		return sex;
	}

	public void setSex(char sex) {
		this.sex = sex;
	}

	@Override
	public String toString() {
		return "Student [name= " + name + ", lastName= " + lastName + ", age= " + age + ", id= " + id + ", sex= " + sex
				+ "]";
	}
	
	
	
	

}
