package com.gmail.ol.bulhakov;

import java.io.Serializable;
import java.util.Arrays;

public class Group implements Serializable{
	private final Student [] students;
	private String groupName;
	

	
	public Group(Student[] students, String groupName) {
		super();
		this.students = students;
		this.groupName = groupName;
		students = new Student [5];
	}
	

	public Group() {
		super();
		students = new Student [5];
	}
	
	

	public String getGroupName() {
		return groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public Student[] getStudents() {
		return students;
	}


	public void addStudentInGroup (Student student) throws GroupOverflowException{
		for (int i = 0; i < students.length; i+=1) {
			if(students[i] == null) {
				students[i]=student;
				return;
			}
		}
		throw new GroupOverflowException("Group is full");
	}

	@Override
	public String toString() {
		String group = groupName + System.lineSeparator();
		for(int i =0; i< students.length; i+=1) {
			if(students[i] != null) {
				group = group + students[i] + ";" + System.lineSeparator();
			}
		}return group;
	}
	
	
	
}
