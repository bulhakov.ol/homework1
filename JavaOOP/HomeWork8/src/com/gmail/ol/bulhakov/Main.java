package com.gmail.ol.bulhakov;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Main {

	public static void main(String[] args) {
		Student st1 = new Student("John", "Smith", 18, 1,'M');
		Student st2 = new Student("Neo", "Matrix", 19, 2,'M');
		Student st3 = new Student("Trinity", "Black", 19, 3,'W');
		Student st4 = new Student("Pamela", "Anderson", 20, 4,'W');
		Student st5 = new Student("Ivo", "Bobul", 24, 5,'M');
		Student st6 = new Student("Cris", "Isaak", 20, 6,'M');
		Student st7 = new Student("Jack", "Sparrow", 21, 7,'M');
		Student st8 = new Student("Albert", "Einshtain", 40, 8,'M');
		Student st9 = new Student("Muka", "Thecow", 17, 9,'W');
		Student st10 = new Student("Peppa", "Pig", 31, 10,'W');
		Student st11 = new Student("Katy", "Perry", 22, 11,'W');
		Student st12 = new Student("Johny", "Mnemonick", 26, 12,'M');
		Student st13 = new Student("Pablo", "Escobar", 51, 13,'M');
		Student st14 = new Student("Lupsen", "Pupsen", 78, 14,'M');
		Student st15 = new Student("Zoya", "Kosmodemyanskaya", 16, 1,'W');
		
		Group film123 = new Group();
		Group mix345 = new Group();
		Group music458 = new Group();
		film123.setGroupName("FILM-1-21");
		mix345.setGroupName("MIX-3-45");
		music458.setGroupName("MUSIC-4-58");
		
		try {
			film123.addStudentInGroup(st1);
			film123.addStudentInGroup(st2);
			film123.addStudentInGroup(st3);
			film123.addStudentInGroup(st4);
			film123.addStudentInGroup(st7);
			music458.addStudentInGroup(st5);
			music458.addStudentInGroup(st6);
			music458.addStudentInGroup(st9);
			music458.addStudentInGroup(st11);
			music458.addStudentInGroup(st12);
			mix345.addStudentInGroup(st8);
			mix345.addStudentInGroup(st10);
			mix345.addStudentInGroup(st13);
			mix345.addStudentInGroup(st14);
			mix345.addStudentInGroup(st15);
			
		} catch (GroupOverflowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Group [] groupDB = new Group [] {film123,mix345,music458};
		GroupDataBase facultet = new GroupDataBase("Факультет Культуры", groupDB);
		System.out.println(facultet);
		GroupDataBase newFacultet = null;
		System.out.println("----------------------------------------------------------");
		System.out.println("------ДАННЫЕ--ПОСЛЕ--СЕРИАЛИЗАЦИИ--И--ДЕСИРАЛИЗАЦИИ-------");
		System.out.println("----------------------------------------------------------");
	
		
		
		SaveFacultet newSaveTake = new SaveFacultet();
		//СЕРИАЛИЗАЦИЯ
		
		newSaveTake.saveDb(facultet);
		//ДЕСИРАЛИЗАЦИЯ
		
		newFacultet = newSaveTake.takeFromFileFacultet(newFacultet);
		
		System.out.println(newFacultet);
		
		
		
	
		
		
		

	}

}
