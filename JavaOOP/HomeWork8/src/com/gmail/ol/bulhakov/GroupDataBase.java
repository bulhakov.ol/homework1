package com.gmail.ol.bulhakov;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;

public class GroupDataBase implements Serializable{
	private String dataBaseName;
	private Group [] groupDB;
	
	public GroupDataBase(String dataBaseName, Group[] groupDB) {
		super();
		this.dataBaseName = dataBaseName;
		this.groupDB = groupDB;
	}

	public GroupDataBase() {
		super();
	}

	public String getDataBaseName() {
		return dataBaseName;
	}

	public void setDataBaseName(String dataBaseName) {
		this.dataBaseName = dataBaseName;
	}

	public Group[] getGroupDB() {
		return groupDB;
	}

	public void setGroupDB(Group[] groupDB) {
		this.groupDB = groupDB;
	}
	

	

	@Override
	public String toString() {
		return "GroupDataBase [dataBaseName=" + dataBaseName + ", groupDB=" + Arrays.toString(groupDB) + "]";
	}
	
	
	

}
