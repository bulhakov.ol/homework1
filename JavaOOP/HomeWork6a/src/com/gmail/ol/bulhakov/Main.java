package com.gmail.ol.bulhakov;

import java.math.BigInteger;
import java.util.Random;



public class Main {

	public static void main(String[] args) {
		int [] arrayNumbers = new int [1000000];
		for (int i =0; i< arrayNumbers.length; i+=1) {
			arrayNumbers[i]=(int)(Math.random()*10);
		}
		//одним потоком
		long startTime;
		long endTime;
		BigInteger uniqueThreadSum= BigInteger.ZERO;
		startTime = System.nanoTime();
		for(int i = 0; i< arrayNumbers.length; i+=1) {
			uniqueThreadSum= uniqueThreadSum.add(BigInteger.valueOf(arrayNumbers[i]));
		}
		endTime = System.nanoTime();
		long timeOfCalcolatingUniqueThread= endTime - startTime;
		System.out.println("Сумма элементов массива равна : " + uniqueThreadSum + "; время вычисления: " + timeOfCalcolatingUniqueThread + " наносекунд");
		// многопоточный расчет
		
		long multipyStartTime;
		long multiplyEndTime;
		BigInteger multiplyThreadSum = BigInteger.ZERO;
		
		multipyStartTime =System.nanoTime();
		
		CalcSumArray one =  new CalcSumArray(arrayNumbers, 0, arrayNumbers.length/5);
		CalcSumArray two =  new CalcSumArray(arrayNumbers, arrayNumbers.length/5,arrayNumbers.length/5*2);
		CalcSumArray three =  new CalcSumArray(arrayNumbers, arrayNumbers.length/5*2,arrayNumbers.length/5*3);
		CalcSumArray four =  new CalcSumArray(arrayNumbers, arrayNumbers.length/5*3,arrayNumbers.length/5*4);
		CalcSumArray five =  new CalcSumArray(arrayNumbers, arrayNumbers.length/5*4,arrayNumbers.length);
		
		Thread th1 = new Thread(one);
		Thread th2 = new Thread(two);
		Thread th3 = new Thread(three);
		Thread th4 = new Thread(four);
		Thread th5 = new Thread(five);
	
		
		
		th1.start();
		th2.start();
		th3.start();
		th4.start();
		th5.start();
	
		
		try {
			th1.join();
			th2.join();
			th3.join();
			th4.join();
			th5.join();
		
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		multiplyThreadSum =multiplyThreadSum.add(one.getSum()).add(two.getSum()).add(three.getSum()).add(four.getSum()).add(five.getSum());
	
		multiplyEndTime = System.nanoTime();
		
		long resultTimeMulty = multiplyEndTime - multipyStartTime;
		
		System.out.println("Сумма элементов массива равна : " + multiplyThreadSum + "; время вычисления: " + resultTimeMulty + " наносекунд");
	}
	
	

}
