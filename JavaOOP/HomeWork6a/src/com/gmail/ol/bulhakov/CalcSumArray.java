package com.gmail.ol.bulhakov;

import java.math.BigInteger;

public class CalcSumArray implements Runnable{
	private int [] numberArray;
	private int start;
	private int end;
	private BigInteger sum =BigInteger.ZERO;
	
	
	public CalcSumArray(int[] numberArray, int start, int end) {
		super();
		this.numberArray = numberArray;
		this.start = start;
		this.end = end;
		
	
		
	}
	public BigInteger getSum() {
		return sum;
	}
	@Override
	public void run () {
		for(int i = start; i< end; i+=1) {
			sum = sum.add(BigInteger.valueOf(numberArray[i]));
		}
	}
	
	



	
	
}
