package com.gmail.ol.bulhakov;

public class Human implements Cloneable{
	private String lastName;
	
	

	public Human(String lastName) {
		super();
		this.lastName = lastName;
	}



	public Human() {
		super();
		// TODO Auto-generated constructor stub
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	@Override
	public String toString() {
		return "Human [lastName=" + lastName + "]";
	}



	@Override
	protected Human clone() {
		try {
			return (Human) super.clone();
		}catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
	

	
	

}
