package com.gmail.ol.bulhakov;

import java.util.ArrayDeque;

public class Main {

	public static void main(String[] args) {
		Human sheldon = new Human("Sheldon");
		Human leonard = new Human("Leonard");
		Human volovitc = new Human("Volovitc");
		Human kutrapalli = new Human("Kutrapalli");
		Human penny = new Human("Penny");
		
		ArrayDeque<Human> humans = new ArrayDeque<Human>();
		humans.addLast(sheldon);
		humans.addLast(leonard);
		humans.addLast(volovitc);
		humans.addLast(kutrapalli);
		humans.addLast(penny);
		
		System.out.println(humans);
		System.out.println("-----------------------------------------");
		System.out.println("---------AFTER--DOUBLE--COLA-------------");
		System.out.println("-----------------------------------------");
		QuequeCola colaParty = new QuequeCola();
		ArrayDeque<Human> newList =colaParty.colaList(humans, 2);
		System.out.println(newList);
	}

}
