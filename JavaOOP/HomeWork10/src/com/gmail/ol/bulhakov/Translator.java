package com.gmail.ol.bulhakov;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Translator {

	private Map<String, String> vocabulary = new HashMap<String, String>();

	public Translator() {
		super();
		vocabularyCreator();
	}

	public Map<String, String> getVocabulary() {
		return vocabulary;
	}

	public void vocabularyCreator() {
		vocabulary.put("Metallica", "Металлика");
		vocabulary.put("is", "это");
		vocabulary.put("an", "a");
		vocabulary.put("American", "aмериканский");
		vocabulary.put("heavy", "тяжёлый");
		vocabulary.put("metal", "металл");
		vocabulary.put("band", "группа");
		vocabulary.put("was", "был");
		vocabulary.put("formed", "сформировано");
		vocabulary.put("in", "в");
		vocabulary.put("Los", "Лос");
		vocabulary.put("Angeles", "Анджелес");

	}

	public void setNewWordsInVocabulary(String eng, String rus) {
		vocabulary.put(eng, rus);
		System.out.println("Вы добавили новую пару в словарь : " + eng + " -> " + rus);
		System.out.println("-----------------------------------------------");
	}

	public void saveVocabularySerialization() {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("saveMap.txt"))) {
			oos.writeObject(vocabulary);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Map<String, String> getVocabularyDeseralization() {
		Map<String, String> map;
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("saveMap.txt"))) {
			try {
				map = (HashMap) ois.readObject();
				return map;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void translator(File fileIn) {
		String englishText = " ";
		StringBuilder sb = new StringBuilder();
		StringBuilder sbr = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader(fileIn))) {
			for (; (englishText = br.readLine()) != null;) {
				sb.append(englishText);
				sb.append(System.lineSeparator());
			}
			englishText = sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String[] arrEngText = englishText.split(" ");

		String finalText = "";
		for (int i = 0; i < arrEngText.length; i += 1) {
			if (vocabulary.get(arrEngText[i]) != null) {
				sbr.append(vocabulary.get(arrEngText[i]) + " ");
			} else {
				sbr.append(arrEngText[i] + " ");
			}
		}
		finalText = sbr.toString();
		try (PrintWriter pw = new PrintWriter(new File("TranslatedToRussian.txt"))) {
			pw.print(finalText);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		return "Translator [vocabulary=" + vocabulary + "]";
	}

}
