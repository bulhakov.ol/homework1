package com.gmail.ol.bulhakov;

import java.io.File;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		File englishFile = new File("/Users/oleksandrbulhakov/Desktop/hw/JavaOOP/HomeWork10/english.txt");
		Translator translator = new Translator();
		translator.setNewWordsInVocabulary("Rock", "Рок");
		System.out.println(translator);
		System.out.println("-----------------------------------------------");
		translator.saveVocabularySerialization();
		translator.translator(englishFile);
		Map<String, String> newVocabularty = translator.getVocabularyDeseralization();
		System.out.println("---------DESERIALIZED--VOCABULARY--------------"+System.lineSeparator()+newVocabularty);
	}

}
