package com.gmail.ol.bulhakov;

import java.io.File;
import java.text.SimpleDateFormat;

public class ScanThread implements Runnable {
	private File folder;
	private int listLength;


	public ScanThread(File folder, int listLength) {
		super();
		this.folder = folder;
		this.listLength = listLength;
	}

	private String scanFolder () {
		if (folder == null) {
			return "Folder Error";
		}
		
		StringBuilder sb = new StringBuilder();
		File [] fileList = folder.listFiles();
		String errorMessage = "";
		if ( listLength != fileList.length) {
			errorMessage = "Date Changed";
		}
		else {
			errorMessage = "Date without Changes";
		}
		for (int i =0; i< fileList.length; i+=1) {
			sb.append(fileList[i].getName() + "\t" + fileList[i].length()).append(System.lineSeparator());
			sb.append(errorMessage);
		}
		return sb.toString();
	}

	@Override
	public void run() {
	 Thread th = Thread.currentThread();
	 SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
	 for(;!th.isInterrupted();) {
		 System.out.println(sdf.format(System.currentTimeMillis()));
		 System.out.println(scanFolder());
		 System.out.println();
		 try {
			 Thread.sleep(1000);
		 }catch (InterruptedException e) {
			break;
		}
	 }
		
	}
	

}
