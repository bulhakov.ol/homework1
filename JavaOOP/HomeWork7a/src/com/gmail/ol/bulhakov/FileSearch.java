package com.gmail.ol.bulhakov;

import java.io.File;

public class FileSearch implements Runnable{
	
	private File [] fileDirectory;
	private String fileName;
	



	
	public FileSearch(File[] fileDirectory, String fileName) {
		super();
		this.fileDirectory = fileDirectory;
		this.fileName = fileName;
	}
	

	public FileSearch() {
		super();
	}


	public File[] getFileDirectory() {
		return fileDirectory;
	}


	public void setFileDirectory(File[] fileDirectory) {
		this.fileDirectory = fileDirectory;
	}


	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	private void searchFile () {
		for (int i = 0; i< fileDirectory.length; i +=1) {
			if(fileDirectory[i] != null && fileDirectory[i].isFile()) {
				if(fileDirectory[i].getName().startsWith(fileName)) {
					System.out.println(fileDirectory[i].getName() +" "+ fileDirectory[i].getAbsolutePath());
				}
			}
			if  (fileDirectory[i] != null && fileDirectory[i].isDirectory()) {
				Thread th = new Thread (new SubSearch(fileDirectory[i], fileName));
				th.start();
				
				
			}
			
			
		}
	}

	@Override
	public void run() {
		
	}
	
	

}
