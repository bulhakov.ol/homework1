package com.gmail.ol.bulhakov;

import java.io.File;

public class SubSearch implements Runnable{
	private File fileDir;
	private String searchName;
	
	
	public SubSearch(File fileDir, String searchName) {
		super();
		this.fileDir = fileDir;
		this.searchName = searchName;
	}

	public SubSearch() {
		super();
	}

	public File getFileDir() {
		return fileDir;
	}

	public void setFileDir(File fileDir) {
		this.fileDir = fileDir;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}
	private void directorySearch (File file) {
		Thread th = Thread.currentThread();
		File [] arr = file.listFiles();
		for (int i = 0; i< arr.length; i +=1) {
			if(arr[i] != null && arr[i].isFile()) {
				if(arr[i].getName().startsWith(searchName)) {
					System.out.println(arr[i].getName() +" "+ arr[i].getAbsolutePath() + " ; " + th);
				}
			}
			if  (arr[i] != null && arr[i].isDirectory()) {
				
				Thread th1 = new Thread (new SubSearch(arr[i], searchName));
				th1.start();
				
			}
			
			
		}
	}

	@Override
	public void run() {
		
		directorySearch(fileDir);
		
	}
	
	
	
	
}
