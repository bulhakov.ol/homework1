package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		Random rn = new Random();

		for (int i = 0; i < 100; i += 1) {
			list.add(rn.nextInt(100));
		}

		System.out.println(list);
		System.out.println("----------------------------------");

		Stream<Integer> stream = list.stream().filter(a -> a > 10).sorted(Main::lastNumberComparator);

		stream.forEach(a -> System.out.println(a));

	}

	public static int lastNumberComparator(Integer int1, Integer int2) {

		int int12 = int1 % 10;
		int int22 = int2 % 10;

		return int12 - int22;
	}

}
