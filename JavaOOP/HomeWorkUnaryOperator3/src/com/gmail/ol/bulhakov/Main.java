package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class Main {

	public static void main(String[] args) {
		List<String> listString = new ArrayList<String>(List.of("Him","Metallica","Nirvana","U-2","Kiss","Sub Velum"));
		System.out.println(listString);
		System.out.println("---------------------------------------");
		UnaryOperator<List<String>> unOp = Main :: newListCreator;
		List<String> newList = unOp.apply(listString);
		System.out.println(newList);

	}
	
	public static List<String> newListCreator (List<String> list){
		List<String> resultList = new ArrayList<String>();
		for(int i =0; i<list.size(); i+=1) {
			if(list.get(i).length() > 5) {
				resultList.add(list.get(i));
			}
		}return resultList;
	}

}
