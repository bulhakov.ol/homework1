package com.gmail.ol.bulhakov;

import java.io.File;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		File fl = new File("/Users/oleksandrbulhakov/Desktop/hw/JavaOOP");
		
		String result = Stream.of(fl.listFiles())
				.filter(a->a.isFile())
				.max((a,b)->(int)(a.length()-b.length()))
				.get()
				.getAbsolutePath();
		
		System.out.println(result);
				
				

	}

}
