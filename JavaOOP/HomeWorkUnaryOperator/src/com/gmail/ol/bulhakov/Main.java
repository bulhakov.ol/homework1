package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.UnaryOperator;

public class Main {

	public static void main(String[] args) {
		List<Integer> intList = new ArrayList<Integer>();
		Random rn = new Random();
		for (int i =0; i< 100; i +=1) {
			intList.add(rn.nextInt(100));
		}
		System.out.println(intList);
		System.out.println("------------------------------------");
		
		UnaryOperator<Integer> unOp = a-> {
			if(a %2 != 0) {
				return 0;
			}else {
				return a;
			}
		};
		
		intList.replaceAll(unOp);
		System.out.println(intList);

	}

}
