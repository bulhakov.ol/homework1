package com.gmail.ol.bulhakov;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class CopyFileCreator {
	public static void copyCreator (File in, File out) throws IOException {
		byte [] buffer = new byte [1024*1024];
		int readByte = 0;
		try (FileInputStream is = new FileInputStream(in); FileOutputStream os = new FileOutputStream(out)){
			for (;(readByte=is.read(buffer))>0;) {
				os.write(buffer, 0, readByte);
			}
		}catch (IOException e) {
			throw e;
		}
	}

public static int copyFileByType (File folderIn, File folderOut, String fileType) throws IOException {
	File [] folder = folderIn.listFiles();
	int copiedFiles = 0;
	for (int i = 0; i < folder.length; i += 1) {
		if(folder[i].isFile() && folder[i].getName().endsWith(fileType)) {
			File fileOut = new File (folderOut, folder[i].getName());
			copyCreator(folder[i], fileOut);
			copiedFiles += 1;
		}
	}
	return copiedFiles;
}
}