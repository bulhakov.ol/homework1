package com.gmail.ol.bulhakov;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiPredicate;



public class Main {

	public static void main(String[] args) {
		Map<Integer, String> someMap = new HashMap<Integer, String>();
		someMap.put(1, "Metallica");
		someMap.put(2,"Nirvana");
		someMap.put(3, "Him");
		someMap.put(4, "Kiss");
		someMap.put(5, "Lacrimosa");
		someMap.put(6, "Slayer");
		someMap.put(7, "Testament");
		someMap.put(10,"Evanesence");
		
		System.out.println("----------------MAP--BEFORE---DELETE---------------");
		someMap.forEach((key,value) -> System.out.println(key + "  ->  " + value));
		
		BiPredicate<Integer, String> biPr = (num,str) -> str.length() != num;
		someMap.entrySet().removeIf(obj -> biPr.test(obj.getKey(), obj.getValue()));
		
		System.out.println("----------------MAP--AFTER---DELETE---------------");
		someMap.forEach((key,value) -> System.out.println(key + "  ->  " + value));
	
	}

}
