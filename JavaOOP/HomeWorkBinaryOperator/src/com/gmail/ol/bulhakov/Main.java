package com.gmail.ol.bulhakov;

import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {

		BinaryOperator<String> binOp = Main::longestString;
		System.out.println(binOp.apply("Metallica", "Nirvana"));
	}

	public static String longestString(String st1, String st2) {
		if (st1.length() > st2.length()) {
			return st1;
		}
		return st2;
	}

}
