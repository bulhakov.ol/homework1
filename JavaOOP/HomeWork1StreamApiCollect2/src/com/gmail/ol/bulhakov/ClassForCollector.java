package com.gmail.ol.bulhakov;

import java.util.HashMap;
import java.util.Map;

import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class ClassForCollector implements Collector<String, Map<Integer, String>, Map<Integer, String>>{
	private Predicate<String> predicate;

	public ClassForCollector(Predicate<String> predicate) {
		super();
		this.predicate = predicate;
	}

	public ClassForCollector() {
		super();
	}

	public Predicate<String> getPredicate() {
		return predicate;
	}

	public void setPredicate(Predicate<String> predicate) {
		this.predicate = predicate;
	}

	@Override
	public Supplier<Map<Integer, String>> supplier() {
		
		return HashMap :: new;
	}

	@Override
	public BiConsumer<Map<Integer, String>, String> accumulator() {
		
		return (a,b) ->{
			if(predicate.test(b)) {
				
				Integer key = b.length();
				a.put(key, b);
				
			}
		};
	}

	@Override
	public BinaryOperator<Map<Integer, String>> combiner() {
		// TODO Auto-generated method stub
		return (map1, map2) -> {
			Map<Integer, String> result= new HashMap<Integer, String>(map1);
			map2.forEach((key,value)->{
				result.put(key, value);
			});
			return result;
		};
		}

	@Override
	public Function<Map<Integer, String>, Map<Integer, String>> finisher() {
		// TODO Auto-generated method stub
		return Function.identity();
	}

	@Override
	public Set<Characteristics> characteristics() {
		// TODO Auto-generated method stub
		return Set.of(Characteristics.IDENTITY_FINISH,Characteristics.UNORDERED);
	}
	
	

}
