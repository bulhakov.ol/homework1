package com.gmail.ol.bulhakov;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Main {

	public static void main(String[] args) {
		List<String> stringWithNumber = new ArrayList<String>();

		Consumer<String> cons = a -> {
			boolean stringHasNumber = false;
			char[] arrNumChar = new char[] { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
			char[] arrSt = a.toCharArray();
			for (int i = 0; i < arrSt.length; i += 1) {
				for (int j = 0; j < arrNumChar.length; j += 1) {
					if (arrSt[i] == arrNumChar[j]) {
						stringHasNumber = true;
						break;
					}
				}
			}
			if (stringHasNumber) {
				stringWithNumber.add(a);
			}
		};

		String st1 = "Metallica";
		String st2 = "Blink-182";
		String st3 = "The Gatharing";
		String st4 = "Lacrimosa";
		String st5 = "U-2";
		String st6 = "Black Sabbath";

		List<String> listString = new ArrayList<String>(List.of(st1, st2, st3, st4, st5, st6));
		System.out.println(listString);

		listString.forEach(cons);

		System.out.println(stringWithNumber);

	}

}
