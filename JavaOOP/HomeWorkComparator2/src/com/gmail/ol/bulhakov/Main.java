package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;


public class Main {

	public static void main(String[] args) {
		List<Integer> intList = new ArrayList<Integer>();
		Random rn = new Random();
		
		for (int i =0; i< 1000; i+=1) {
			if (i%7!=0) {
				intList.add(-rn.nextInt(536));
			}
		
			intList.add(rn.nextInt(555));
		}
		System.out.println(intList);
		System.out.println("-------------------------------------");
		
		
		
		
		Comparator<Integer> comp1 = (a,b) -> Math.abs(a) - Math.abs(b);
		Comparator<Integer> comp2 = (a,b) -> b - a;
		Comparator<Integer> compFinal = comp1.thenComparing(comp2);
		
		
		Collections.sort(intList, compFinal);
		System.out.println(intList);
	}
	
	

}
