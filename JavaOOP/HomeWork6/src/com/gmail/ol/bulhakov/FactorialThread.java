package com.gmail.ol.bulhakov;

import java.math.BigInteger;

public class FactorialThread implements Runnable{
	private int number;

	public FactorialThread(int number) {
		super();
		this.number = number;
	}

	public int getNumber() {
		return number;
	}
	private BigInteger calcolateFactorial (int number) {
		BigInteger factorial = BigInteger.ONE;
		for (int i = 1; i <= number; i+=1) {
			factorial = factorial.multiply(BigInteger.valueOf(i));
		}return factorial;
	}
	@Override
	public void run() {
		Thread th = Thread.currentThread();
		System.out.println(th.getName()+"; факториал числа: "+number + " равен: "+calcolateFactorial(number));
	}
}
