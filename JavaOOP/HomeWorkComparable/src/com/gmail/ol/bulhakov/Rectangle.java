package com.gmail.ol.bulhakov;


public class Rectangle implements Comparable<Rectangle>{
	
	private int longRect;
	private int heighRect;

	


	public Rectangle(int longRect, int heighRect) {
		super();
		this.longRect = longRect;
		this.heighRect = heighRect;
	}


	public Rectangle() {
		super();
	}


	public int getLongRect() {
		return longRect;
	}


	public void setLongRect(int longRect) {
		this.longRect = longRect;
	}


	public int getHeighRect() {
		return heighRect;
	}


	public void setHeighRect(int heighRect) {
		this.heighRect = heighRect;
	}
	
	

	public int area () {
		return longRect * heighRect;
	}
	
	@Override
	public String toString() {
		return  "Area: " + area() ;
	}




	@Override
	public int compareTo(Rectangle o) {
		if(o == null) {
			throw new NullPointerException();
		}
		return Integer.compare(this.area(), o.area());
	}


	
	
	
}
