package com.gmail.ol.bulhakov;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class CatDeleter {

	public int catIndex(List<Cat> ls, char ch) {
		Collections.sort(ls, (o1,o2)-> o1.getName().compareTo(o2.getName()));
		int index = -1;
		for (int i = 0; i < ls.size(); i += 1) {
			if (ls.get(i).getName().startsWith(Character.toString(ch))) {
				
				index = i;
				break;
			}

		}
		return index;
	}
	
	public void catEliminator ( List <Cat> ls, int number, char ch) {
	
		
		int index = catIndex(ls, ch);
		Predicate<Cat> pr = a-> ls.indexOf(a) >= index;
		Predicate<Cat> pr2 = a -> a.getAge() < number;
		ls.removeIf(pr.and(pr2));
		
	
	
	}

}
