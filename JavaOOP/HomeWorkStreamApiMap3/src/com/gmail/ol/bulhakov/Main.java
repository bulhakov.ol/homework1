package com.gmail.ol.bulhakov;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		File fl = new File("/Users/oleksandrbulhakov/Desktop/hw/JavaOOP/HomeWorkStreamApiMap");

		List<String> list = Stream.of(fl.listFiles()).filter(file -> file.isFile())
				.filter(file -> file.getName().endsWith("txt")).map(file -> file.getName())
				.collect(Collectors.toList());
		System.out.println(list);

	}

}
