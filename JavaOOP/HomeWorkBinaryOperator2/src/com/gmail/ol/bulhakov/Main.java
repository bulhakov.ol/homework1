package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {
		List<Integer> firstList = List.of(1, 2, 5, 7, 55, 13, 6, 88, 9, 33, 4, 99);
		System.out.println(firstList);
		System.out.println("---------------------------------------------");
		List<Integer> secondList = List.of(5, 53, 22, 88, 44, 96, 31, 8, 99, 123, 15);
		System.out.println(secondList);
		System.out.println("---------------------------------------------");

		BinaryOperator<List<Integer>> binOp = (a, b) -> {
			List<Integer> resulList = new ArrayList<Integer>();
			for (int i = 0; i < a.size(); i += 1) {
				for (int j = 0; j < b.size(); j += 1) {
					if (a.get(i) == b.get(j)) {
						resulList.add(a.get(i));
					}
				}
			}
			return resulList;

		};

		List<Integer> resultList = binOp.apply(firstList, secondList);
		System.out.println(resultList);

	}

}
