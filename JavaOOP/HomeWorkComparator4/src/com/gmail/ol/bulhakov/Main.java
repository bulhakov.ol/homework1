package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

public class Main {

	public static void main(String[] args) {
		List<Integer> intList = new ArrayList<Integer>();
		Random rn = new Random();
		for (int i = 0; i < 1000; i += 1) {
			intList.add(rn.nextInt(1000));
		}
		System.out.println(intList);
		System.out.println("---------------------------");

		Function<Integer, Integer> fun = a -> {
			for (int i = 2; i < a; i += 1) {
				if ((a % i) == 0) {
					return 0;
				}
			}
			return a;
		};
		
		Comparator<Integer> comp = (a,b) -> fun.apply(a) -fun.apply(b);
		Comparator<Integer> comp2 = (a,b) -> b - a;
		System.out.println(Collections.max(intList, comp.thenComparing(comp2)));
	}

}
