package com.gmail.ol.bulhakov;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		String[] xml = new String[] { "<dependency>", "<groupId>junit</groupId>", "<artifactId>junit</artifactId>",
				"<version>4.4</version>", "<scope>test</scope>", "</dependency> <dependency>",
				"<groupId>org.powermock</groupId>", "<artifactId>powermock-reflect</artifactId>",
				"<version>3.2</version>", "</dependency>" };

		List<String> result = Arrays.stream(xml).filter(a -> a.contains("<groupId>"))
				.map(a -> a.replaceAll("<groupId>", "")).map(a -> a.replaceAll("</groupId>", ""))
				.collect(Collectors.toList());

		System.out.println(result);
	}

}
