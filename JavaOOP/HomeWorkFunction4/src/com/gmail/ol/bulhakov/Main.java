package com.gmail.ol.bulhakov;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class Main {

	public static void main(String[] args) {
		String firstString = "Hello Jonny you are my friend";
		String secondString = "Hello mr.Black you are my adversary";
		textArrCreator(firstString, secondString);
		BiFunction<String, String, String[]> func = Main :: textArrCreator;
		String [] stringResult = func.apply(firstString, secondString);
		System.out.println(Arrays.toString(stringResult));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public static String [] textArrCreator (String st1, String st2) {
		String [] stOne = st1.split(" ");
		String [] stTwo = st2.split(" ");
		List<String> list = new ArrayList<String>();
		for(int i = 0; i< stOne.length; i+=1) {
			for(int j=0; j< stTwo.length; j+=1) {
				if(stOne[i].equals(stTwo[j])) {
					list.add(stTwo[j]);
				}
			}
		}
		String [] arrString = new String [list.size()];
		arrString = list.toArray(arrString);
		return arrString;
	}

}
