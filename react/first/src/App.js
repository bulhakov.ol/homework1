
import './App.css';
import Test from './test';
import Header from './header/header';

const headerData = {
  site_name : "some text for header",
  nav : [
    {"link" : "nav1", "text" : "my link"},
    {"link" : "nav2", "text" : "my link2"},
    {"link" : "nav3", "text" : "my link3"},
  ]
}

function App() {
  return (
    <>
    <Test/>
    <Header data={headerData}></Header>
    </>
  );
}

export default App;
