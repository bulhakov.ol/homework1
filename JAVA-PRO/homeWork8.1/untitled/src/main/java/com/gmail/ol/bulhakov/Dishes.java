package com.gmail.ol.bulhakov;

import javax.persistence.*;

@Entity
public class Dishes {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private Integer price;
    private Integer weigh;

    private boolean sales;


    public Dishes() {
    }

    public Dishes(String name, Integer price, Integer weigh, boolean sales) {
        this.name = name;
        this.price = price;
        this.weigh = weigh;
        this.sales = sales;
    }

    public String getName() {
        return name;
    }

    public boolean isSales() {
        return sales;
    }

    public void setSales(boolean sales) {
        this.sales = sales;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getWeigh() {
        return weigh;
    }

    public void setWeigh(Integer weigh) {
        this.weigh = weigh;
    }


    @Override
    public String toString() {
        return "Dishes{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", weigh=" + weigh +
                ", sales=" + sales +
                '}';
    }
}
