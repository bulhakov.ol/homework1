package com.gmail.ol.bulhakov;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    static EntityManagerFactory emf;
    static EntityManager em;
   static Dishes plov = new Dishes("Plov", 5, 200, false);
    static Dishes pelmeni = new Dishes("Pelmeni", 7, 320, true);
    static Dishes shashlik = new Dishes("Shashlik", 10, 350, false);
    static Dishes lasania = new Dishes("Lasania", 12, 230, true);
    static Dishes mqxiBurger = new Dishes("Maxy Burger", 20, 1000, true);

    static Dishes [] menu = new Dishes[]{plov,pelmeni,shashlik,lasania,mqxiBurger};


    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);
        emf = Persistence.createEntityManagerFactory("com.gmail.ol.bulhakov");
        em = emf.createEntityManager();
        try {
            while (true) {
                System.out.println("ADD DISH                         => PUSH \" 1\"");
                System.out.println("ADD ALL DISHES                   => PUSH \" 2\"");
                System.out.println("SHOW MENU                        => PUSH \" 3\" ");
                System.out.println("PRICE LESS  YOUR PRICE           => PUSH \" 4\"");
                System.out.println("ADD DISHES LESS TOTAL WEIGHT 1kg => PUSH \" 5\"");
                System.out.println("DISHES IN SALES                  => PUSH \" 6\"");

                int responce = sc.nextInt();
                if( responce == 1){
                    addDish();
                } else if (responce == 2) {
                    addAllDishes();
                }else if (responce == 3) {
                    showMenu();
                }else if (responce == 4) {
                    priceLEss();
                }else if (responce == 5) {
                    addDishesWeightLess1kg();
                }else if (responce == 6) {
                    dishesInSALES();
                }


            }

            }finally {
            em.close();
            emf.close();
        }



    }

    public static void addDish() {
        Dishes dish = choseDish();
        em.getTransaction().begin();
        try {
            em.persist(dish);
            em.getTransaction().commit();
            System.out.println("added DISH" + dish.getName());
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }
    public static void addAllDishes (){
        em.getTransaction().begin();

       for(int i =0; i<menu.length; i+=1){
           em.persist(menu[i]);
       }
       em.getTransaction().commit();
       System.out.println("-----ALL--DISHES--ADDED-----");

    }
    public static  void priceLEss (){
        Scanner sc = new Scanner(System.in);
        System.out.println("ADD MAX PRICE");
       String maxPrice = sc.nextLine();
       Query query =em.createQuery("SELECT d FROM Dishes d WHERE d.price <= " + maxPrice,Dishes.class );
        List<Dishes> list= query.getResultList();
        System.out.println("------DISHES--WITH--PRICE--LESS--" +maxPrice+"------");
        list.forEach(a->System.out.println(a.getName().toUpperCase() + "\t PRICE: "+a.getPrice() + "\t WEIGH:" + a.getWeigh()));
        System.out.println("------------------------------------------");
    }



    public static void showMenu (){
        Query query = em.createQuery("SELECT d FROM Dishes d",Dishes.class );
        List<Dishes> menuList = (List<Dishes>) query.getResultList();
        System.out.println("---------------MENU---------------");
        menuList.forEach(a->System.out.println(a.getName().toUpperCase() + "\t PRICE: "+a.getPrice() + "\t WEIGH:" + a.getWeigh()));
        System.out.println("----------------------------------");
    }

    public static void addDishesWeightLess1kg(){
       int totalWeight = 1000;
        int addedWeight = 0;
        em.getTransaction().begin();
       while (addedWeight<totalWeight){
           Dishes dishes = choseDish();
           addedWeight += dishes.getWeigh();
           em.persist(dishes);

        }
       System.out.println("----MAX-----WEIGHT----!KG-----");
       em.getTransaction().commit();
    }

    public static void dishesInSALES (){
        Query query =em.createQuery("SELECT d FROM Dishes d WHERE d.sales = true ",Dishes.class );
        List<Dishes> list= query.getResultList();
        System.out.println("------DISHES--WITH--SALES-----------------");
        list.forEach(a->System.out.println(a.getName().toUpperCase() + "\t PRICE: "+a.getPrice() + "IN SALES: " + a.isSales()));
        System.out.println("------------------------------------------");
    }

    public static Dishes choseDish() {
        Scanner scr = new Scanner(System.in);
        System.out.println("CHOSE DISH");
        System.out.println("PLOV        => 1");
        System.out.println("PELMENI     => 2");
        System.out.println("SHASHLIK    => 3");
        System.out.println("LASAGNA     => 4");
        System.out.println("MAXY BURGER => 5");

        int x = scr.nextInt();
        if (x == 1) {
            return plov;
        } else if (x == 2) {
            return pelmeni;
        } else if (x == 3) {
            return shashlik;
        } else if (x == 4) {
            return lasania;
        } else {
            return mqxiBurger;
        }

    }



}
