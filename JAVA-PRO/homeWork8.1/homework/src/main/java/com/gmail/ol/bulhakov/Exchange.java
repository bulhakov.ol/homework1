package com.gmail.ol.bulhakov;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Exchange {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer euroUahBuy;
    private Integer euroUahSold;

    private Integer usdUahBuy;
    private Integer usdUahSold;

    private Integer usdEuroBuy;
    private Integer usdEuroSold;

    public Exchange() {
    }

    public Exchange(Integer euroUahBuy, Integer euroUahSold, Integer usdUahBuy, Integer usdUahSold) {
        this.euroUahBuy = euroUahBuy;
        this.euroUahSold = euroUahSold;
        this.usdUahBuy = usdUahBuy;
        this.usdUahSold = usdUahSold;

    }

    public Long getId() {
        return id;
    }

    public Integer getEuroUahBuy() {
        return euroUahBuy;
    }

    public void setEuroUahBuy(Integer euroUahBuy) {
        this.euroUahBuy = euroUahBuy;
    }

    public Integer getEuroUahSold() {
        return euroUahSold;
    }

    public void setEuroUahSold(Integer euroUahSold) {
        this.euroUahSold = euroUahSold;
    }

    public Integer getUsdUahBuy() {
        return usdUahBuy;
    }

    public void setUsdUahBuy(Integer usdUahBuy) {
        this.usdUahBuy = usdUahBuy;
    }

    public Integer getUsdUahSold() {
        return usdUahSold;
    }

    public void setUsdUahSold(Integer usdUahSold) {
        this.usdUahSold = usdUahSold;
    }


    @Override
    public String toString() {
        return "Exchange{" +
                "id=" + id +
                ", euroUahBuy=" + euroUahBuy +
                ", euroUahSold=" + euroUahSold +
                ", usdUahBuy=" + usdUahBuy +
                ", usdUahSold=" + usdUahSold +

                '}';
    }
}
