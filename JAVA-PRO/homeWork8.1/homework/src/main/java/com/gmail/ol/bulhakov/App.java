package com.gmail.ol.bulhakov;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;
import java.util.Scanner;


public class App {
    public static EntityManagerFactory emf;
    public static EntityManager em;
    static Client client1 = new Client("Ozzy", "Osbourne", new BankCount(0, 1250, 7640), new Transaction());
    static Client client2 = new Client("Marilyn", "Manson", new BankCount(34, 8765, 2314), new Transaction());
    static Exchange exchange = new Exchange(40, 39, 38, 36);

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        emf = Persistence.createEntityManagerFactory("com.gmail.ol.bulhakov");
        em = emf.createEntityManager();
        addClients(client1);
        addClients(client2);
        addCurrency(exchange);

        try {
            while (true) {
                System.out.println("SHOW CLIENTS                     => PUSH \" 1\"");
                System.out.println("EXCHANGE FOUNDS                  => PUSH \" 2\"");
                System.out.println("SHOW TRANSACTION                 => PUSH \" 3\" ");
                System.out.println("FOUND TRANSFER                   => PUSH \" 4\"");
                System.out.println("SHOW CURRENCY                    => PUSH \" 5\"");


                int responce = sc.nextInt();
                if (responce == 1) {
                    showCliens();
                } else if (responce == 2) {
                    excange();
                } else if (responce == 3) {
                    showTransaction();
                } else if (responce == 4) {
                    foundTransfer();
                }else if (responce == 5) {
                    showCurrency();
                }


            }

        } finally {
            em.close();
            emf.close();
        }


    }

    public static void addClients(Client c) {
        em.getTransaction().begin();
        try {
            em.persist(c);
            em.getTransaction().commit();
            System.out.println("Client :" + c + " Added");

        } catch (Exception e) {
            em.getTransaction().rollback();
            System.out.println("Error!!!");
        }
    }
    public static void addCurrency(Exchange c) {
        em.getTransaction().begin();
        try {
            em.persist(c);
            em.getTransaction().commit();
            System.out.println("Currency :" + c + " Added");

        } catch (Exception e) {
            em.getTransaction().rollback();
            System.out.println("Error!!!");
        }
    }

    public static void showCliens() {
        Query query = em.createQuery("SELECT c FROM Client c", Client.class);
        List<Client> menuList = (List<Client>) query.getResultList();
        String start = "---------------CLIENS---------------";
        String end = "-------------------------------------";
        menuList.forEach(a -> System.out.println(start + System.lineSeparator() + "NAME : " + a.getName()
                + System.lineSeparator()
                + "LASTNAME : " + a.getLastName()
                + System.lineSeparator() + "UAH : " + a.getCount().getUah()
                + System.lineSeparator() + "EURO : " + a.getCount().getEuro()
                + System.lineSeparator() + "USD : " + a.getCount().getUsd()
                + System.lineSeparator() + "MOVIMENTS : " + a.getTransaction().getMoviments()
                + System.lineSeparator() + end));

    }
    public static void showCurrency() {
        Query query = em.createQuery("SELECT c FROM Exchange c", Exchange.class);
        List<Exchange> menuList = (List<Exchange>) query.getResultList();
        String start = "---------------CURRENCY---------------";
        String end = "-------------------------------------";
        menuList.forEach(a -> System.out.println(start + System.lineSeparator() + "EURO BUY : " + a.getEuroUahBuy()
                + System.lineSeparator()
                + "EURO SOLD : " + a.getEuroUahSold()
                + System.lineSeparator() + "USD BUY : " + a.getUsdUahBuy()
                + System.lineSeparator() + "USD SOLD : " + a.getUsdUahSold()
                + System.lineSeparator() + end));

    }


    public static void excange() {
        Scanner sc = new Scanner(System.in);
        Client cl = choseClient();
        System.out.println("WHAT CURRENCY DO YOU WANT EXCHANGE: ");
        int from = choseCount();
        System.out.println("HOW MATCH YOU WANT EXCHANGE");
        int sum = sc.nextInt();
        System.out.println("WHAT TYPE OF CUREENCY YOU WANT TAKE: ");
        int to = choseCount();
        em.getTransaction().begin();
        try {
            if (from == 3 && to == 1) {
                fromEUROtoUAH(cl, sum);
                em.merge(cl);
                em.getTransaction().commit();
            } else if (from == 2 && to == 1) {
                fromUsdToUAH(cl, sum);
                em.merge(cl);
                em.getTransaction().commit();
            } else if (from == 1 && to == 3) {
                fromUAHtoEURO(cl, sum);
                em.merge(cl);
                em.getTransaction().commit();
            } else if (from == 1 && to == 2) {
                fromUAHtoUSD(cl, sum);
                em.merge(cl);
                em.getTransaction().commit();
            }
            System.out.println("EURO TO USD , and , USD TO EURO not possible to change");
        } catch (Exception e) {
            em.getTransaction().rollback();
        }
    }

    public static void showTransaction() {
        Client cl = choseClient();
        System.out.println(cl.getTransaction().getMoviments());
    }

    public static void foundTransfer() {
        Scanner sc = new Scanner(System.in);
        System.out.println("FROM WHAT CLIENT MONEY TRANSFER" + System.lineSeparator() + "----------------------");
        Client cl = choseClient();
        System.out.println("WHAT CLIENT MUST RECIVE MONEY" + System.lineSeparator() + "----------------------");
        Client cl2 = choseClient();
        System.out.println("CHOSE COUNT TYPE" + System.lineSeparator() + "----------------------");
        int count = choseCount();
        System.out.println("HOW MATCH" + System.lineSeparator() + "----------------------");
        int sum = sc.nextInt();
        if (count > 0) {
            if (insufficentResorse(cl, count, sum)) {
                em.getTransaction().begin();
                try {
                    fromClienttoClient(cl, cl2, count, sum);
                    em.merge(cl);
                    em.merge(cl2);
                } catch (Exception e) {
                    em.getTransaction().rollback();
                }
            } else {
                System.out.println("!!!INSUFFICIENT FOUNDS!!!");
            }
        } else {
            System.out.println("ERROR WITH CURRENCYE TRY AGAIN");
            foundTransfer();
        }
    }

    public static Client choseClient() {
        Scanner sc = new Scanner(System.in);
        System.out.println("IF YOU NEED CLIENT OZZY OSBOURNE  =>  1");
        System.out.println("IF YOU NEED CLIENT MARILYN MANSON  =>  2");
        int res = sc.nextInt();
        if (res == 1) {
            return client1;
        } else {
            return client2;
        }


    }

    public static int choseCount() {
        Scanner sc = new Scanner(System.in);
        System.out.println("IF YOU WANT UAH   =>  1");
        System.out.println("IF YOU WANT USD   =>  2");
        System.out.println("IF YOU WANT EURO  =>  3");
        int res = sc.nextInt();
        if (res == 1) {
            return 1;
        } else if (res == 2) {
            return 2;
        } else if (res == 3) {
            return 3;
        }
        return -1;

    }

    public static void fromEUROtoUAH(Client cl, int sum) {
        if (sum < cl.getCount().getEuro()) {
            int euro = cl.getCount().getEuro();
            cl.getCount().setEuro(euro - sum);
            int uah = cl.getCount().getUah();
            cl.getCount().setUah(uah + (sum * exchange.getEuroUahSold()));
            cl.getTransaction().addMoviments("EXCHANGE EURO : " + sum + "TO UAH " + System.lineSeparator());
        }else{
        System.out.println("---------Insufficient found in your count------------");}
    }

    public static void fromUAHtoEURO(Client cl, int sum) {
        if (sum < cl.getCount().getUah()) {
            int uah = cl.getCount().getUah();
            cl.getCount().setUah(uah - sum);
            int euro = cl.getCount().getEuro();
            cl.getCount().setEuro(euro - (sum / exchange.getEuroUahBuy()));
            cl.getTransaction().addMoviments("EXCHANGE UAH: " + sum + "TO EURO " + System.lineSeparator());
        }else {
        System.out.println("---------Insufficient found in your count------------");}
    }

    public static void fromUsdToUAH(Client cl, int sum) {
        if (sum < cl.getCount().getUsd()) {
            int usd = cl.getCount().getUsd();
            cl.getCount().setUsd(usd - sum);
            int uah = cl.getCount().getUah();
            cl.getCount().setUah(uah + (sum * exchange.getUsdUahSold()));
            cl.getTransaction().addMoviments("EXCHANGE USD : " + sum + "TO UAH " + System.lineSeparator());
        }else{
        System.out.println("---------Insufficient found in your count------------");}
    }

    public static void fromUAHtoUSD(Client cl, int sum) {
        if (sum < cl.getCount().getUah()) {
            int uah = cl.getCount().getUah();
            cl.getCount().setUah(uah - sum);
            int usd = cl.getCount().getUsd();
            cl.getCount().setUsd(usd - (sum / exchange.getUsdUahBuy()));
            cl.getTransaction().addMoviments("EXCHANGE UAH: " + sum + " TO USD " + System.lineSeparator());
        }else{
        System.out.println("---------Insufficient found in your count------------");}
    }

    public static void fromClienttoClient(Client from, Client to, int currency, int sum) {
        if (currency == 1) {

            int uah = from.getCount().getUah();
            from.getCount().setUah(uah - sum);
            int uah2 = to.getCount().getUah();
            to.getCount().setUah(uah2 + sum);
            from.getTransaction().addMoviments("TRANFER: " + sum + " TO : "+to.getName()+ " "+to.getLastName()+ System.lineSeparator());
            to.getTransaction().addMoviments("TRANFER: " + sum + " FROM : "+from.getName()+ " "+from.getLastName()+ System.lineSeparator());
        } else if (currency == 2) {


            int usd = from.getCount().getUsd();
            from.getCount().setUsd(usd - sum);
            int usd2 = to.getCount().getUsd();
            to.getCount().setUsd(usd2 + sum);
            from.getTransaction().addMoviments("TRANFER: " + sum + " TO : "+to.getName()+ " "+to.getLastName()+ System.lineSeparator());
            to.getTransaction().addMoviments("TRANFER: " + sum + " FROM : "+from.getName()+ " "+from.getLastName()+ System.lineSeparator());

        } else if (currency == 3) {

            int euro = from.getCount().getEuro();
            from.getCount().setEuro(euro - sum);
            int euro2 = to.getCount().getEuro();
            to.getCount().setEuro(euro2 + sum);
            from.getTransaction().addMoviments("TRANFER: " + sum + " TO : "+to.getName()+ " "+to.getLastName()+ System.lineSeparator());
            to.getTransaction().addMoviments("TRANFER: " + sum + " FROM : "+from.getName()+ " "+from.getLastName()+ System.lineSeparator());

        }
    }

    public static boolean insufficentResorse(Client cl, int count, int sum) {
        if (count == 1) {
            int res = cl.getCount().getUah() - sum;
            if (res > 0) {
                return true;
            }
            return false;
        } else if (count == 2) {
            int res = cl.getCount().getUsd() - sum;
            if (res > 0) {
                return true;
            }
            return false;
        } else if (count == 3) {
            int res = cl.getCount().getEuro() - sum;
            if (res > 0) {
                return true;
            }
            return false;
        }
        System.out.println("---------ERROR-----------");
        return false;
    }
}
