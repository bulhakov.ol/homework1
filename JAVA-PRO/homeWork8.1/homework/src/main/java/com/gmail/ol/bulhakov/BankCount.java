package com.gmail.ol.bulhakov;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BankCount {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Integer uah;
    private Integer usd;
    private Integer euro;

    public BankCount() {
    }

    public BankCount( Integer uah, Integer usd, Integer euro) {
        this.uah = uah;
        this.usd = usd;
        this.euro = euro;
    }

    public Long getId() {
        return id;
    }


    public Integer getUah() {
        return uah;
    }

    public void setUah(Integer uah) {
        this.uah = uah;
    }

    public Integer getUsd() {
        return usd;
    }

    public void setUsd(Integer usd) {
        this.usd = usd;
    }

    public Integer getEuro() {
        return euro;
    }

    public void setEuro(Integer euro) {
        this.euro = euro;
    }

    @Override
    public String toString() {
        return "BankCount{" +
                "id=" + id +
                ", uah=" + uah +
                ", usd=" + usd +
                ", euro=" + euro +
                '}';
    }
}
