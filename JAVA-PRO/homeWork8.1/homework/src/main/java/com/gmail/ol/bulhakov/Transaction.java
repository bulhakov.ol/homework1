package com.gmail.ol.bulhakov;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String moviments;

    public Transaction() {
        String movimement = "";
        this.moviments= movimement;
    }

    public void setMoviments(String moviments) {
        this.moviments = moviments;
    }

    public String getMoviments() {
        return moviments;
    }

    public void addMoviments(String str) {
        moviments += " " + str;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", moviments='" + moviments + '\'' +
                '}';
    }
}
