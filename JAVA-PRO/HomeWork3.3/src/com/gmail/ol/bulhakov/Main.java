package com.gmail.ol.bulhakov;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, IOException, InstantiationException {
        MusicBand metallica = new MusicBand("Metallica",1983);
        MusicBand newMusicBand = new MusicBand();
        SerializatorAndDeserializator.serializator(metallica,"metallica.txt");
        newMusicBand=SerializatorAndDeserializator.deserialization(metallica,"/Users/oleksandrbulhakov/Desktop/hw/JAVA-PRO/HomeWork3.3/metallica.txt");
        System.out.println("-------------RESULT-------------");
        System.out.println(newMusicBand);
    }
}