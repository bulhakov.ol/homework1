package com.gmail.ol.bulhakov;

public class MusicBand {
    @ForSerialization
    private String name;
    @ForSerialization
    private int creationDate;

    private boolean activeOrNot = true;

    public MusicBand(String name, int creationDate) {
        this.name = name;
        this.creationDate = creationDate;
        this.activeOrNot = true;
    }

    public MusicBand() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(int creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isActiveOrNot() {
        return activeOrNot;
    }

    public void setActiveOrNot(boolean activeOrNot) {
        this.activeOrNot = activeOrNot;
    }

    @Override
    public String toString() {
        return "MusicBand{" +
                "name='" + name + '\'' +
                ", creationDate=" + creationDate +
                ", activeOrNot=" + activeOrNot +
                '}';
    }
}
