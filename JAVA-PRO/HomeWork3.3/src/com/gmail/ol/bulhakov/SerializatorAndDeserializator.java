package com.gmail.ol.bulhakov;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


public class SerializatorAndDeserializator {

    public static void serializator(Object obj, String path) throws IllegalAccessException, IOException {
        Class<?> cls = obj.getClass();
        Field[] fields = cls.getDeclaredFields();
        StringBuilder sb = new StringBuilder();
        String fieldsToString;
        for (Field field : fields) {
            if (field.isAnnotationPresent(ForSerialization.class)) {
                if (Modifier.isPrivate(field.getModifiers())) {
                    field.setAccessible(true);
                }
                sb.append(field.getName() + "->");
                if (field.getType() == int.class) {
                    sb.append(field.getInt(obj));
                } else if (field.getType() == String.class) {
                    sb.append((String) field.get(obj));
                }
                sb.append(";");
            }

        }
        fieldsToString = sb.toString();
        Path pth = Paths.get(path);
        Files.write(pth, fieldsToString.getBytes());

        System.out.println("-----SERIALIZATION COMPLETE-----");


    }

    public static <T>T  deserialization(Object obj, String path) throws IOException, InstantiationException, IllegalAccessException {
        StringBuilder sb = new StringBuilder();
        String text = "";
        try (BufferedReader br = new BufferedReader(new FileReader(new File(path)))) {

            for (; (text = br.readLine()) != null; ) {
                sb.append(text);
                sb.append(System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Class<?> cls = obj.getClass();
        T o = (T)cls.newInstance();
        Field[] fields = cls.getDeclaredFields();

        String[] arr = sb.toString().split(";");

        Map<String,String> mp= new HashMap<>();
        for (int i = 0; i < arr.length-1; i += 1) {
          String [] arr2 = arr[i].split("->");

          mp.put(arr2[0],arr2[1]);
        }

        for (Field field:fields) {
            if(field.isAnnotationPresent(ForSerialization.class)){
                if(Modifier.isPrivate(field.getModifiers())){
                    field.setAccessible(true);
                }
                String name = field.getName();

                if(field.getType()==int.class){
                    field.setInt(o,Integer.parseInt(mp.get(name)));
                } else if (field.getType()== String.class) {
                    field.set(o,mp.get(name));
                }
            }
        }System.out.println("-----DESERIALIZATION COMPLETE---");
        return o;





    }
}
