package com.gmail.ol.bulhakov;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
public @interface InFileSaveAnnotation {
    String st ();
}
