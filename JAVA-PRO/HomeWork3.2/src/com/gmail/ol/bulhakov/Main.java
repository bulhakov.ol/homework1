package com.gmail.ol.bulhakov;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        TextContainer tc = new TextContainer();
        Class<?> cls = tc.getClass();
        if(!cls.isAnnotationPresent(InFileSaveAnnotation.class)){
            System.out.println("ANNOTATION ERROR");
        }
        Method [] methods = cls.getDeclaredMethods();
        InFileSaveAnnotation inFileSaveAnnotation= cls.getAnnotation(InFileSaveAnnotation.class);
        String str = inFileSaveAnnotation.st();
        System.out.println(str);

   for (Method m: methods) {
            if(m.isAnnotationPresent(Saver.class)){
               m.invoke(tc,str);
               break;
            }
        }
        System.out.println("OK");
    }
}