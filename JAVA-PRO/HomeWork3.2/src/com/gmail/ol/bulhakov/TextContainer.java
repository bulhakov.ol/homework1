package com.gmail.ol.bulhakov;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@InFileSaveAnnotation(st="/Users/oleksandrbulhakov/Desktop/hw/JAVA-PRO/HomeWork3.2/test.txt")
 class TextContainer {
   String text = "I love JAVA";
    @Saver
    public void saveTextInFile (String st) throws IOException {
        Path path = Paths.get(st);
        Files.write(path, text.getBytes());
    }
}
