import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        Class<?> cls = TestClass.class;
        Method[] methods = cls.getDeclaredMethods();
        for (Method met : methods) {
            if(met.isAnnotationPresent(TestAnnotation.class)){
                TestAnnotation ta = met.getAnnotation(TestAnnotation.class);
                int result = (int) met.invoke(null,ta.a(),ta.b());
                System.out.println(result);
            }
        }
    }
}