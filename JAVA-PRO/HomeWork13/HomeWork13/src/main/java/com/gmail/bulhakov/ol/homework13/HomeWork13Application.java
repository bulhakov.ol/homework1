package com.gmail.bulhakov.ol.homework13;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeWork13Application {

    public static void main(String[] args) {
        SpringApplication.run(HomeWork13Application.class, args);
    }

}
