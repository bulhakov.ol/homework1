package academy.prog;

import jakarta.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

// Req -> (S -> S) -> jsp

public class LoginServlet extends HttpServlet {
    static final String LOGIN = "admin";
    static final String PASS = "admin";
    static final String TEMPLATE ="<html><head></head><body style=\"color :red\">%s</body></html>";



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        int age = Integer.parseInt(request.getParameter("age"));




        if (LOGIN.equals(login) && PASS.equals(password) && age >18 ) {
            HttpSession session = request.getSession(true);
            session.setAttribute("user_login", login);
            session.setAttribute("user_age",age);
            response.sendRedirect("index.jsp");


        } else if (LOGIN.equals(login) && PASS.equals(password) && age <18) {
            PrintWriter pw = response.getWriter();
            pw.println(String.format(TEMPLATE," ERROR AGE LESS 18"));
            pw.println("<a href=\"/login?a=exit\">logout</a>");
        }
        else if (LOGIN.equals(login) && !PASS.equals(password) ) {
            PrintWriter pw = response.getWriter();
            pw.println(String.format(TEMPLATE," ERROR PASSWORD"));
            pw.println("<a href=\"/login?a=exit\">logout</a>");
        }
        else if (!LOGIN.equals(login) && PASS.equals(password) ) {
            PrintWriter pw = response.getWriter();
            pw.println(String.format(TEMPLATE," LOGIN ERROR"));
            pw.println("<a href=\"/login?a=exit\">logout</a>");
        }
        else if (!LOGIN.equals(login) && !PASS.equals(password) ) {
            PrintWriter pw = response.getWriter();
            pw.println(String.format(TEMPLATE," LOGIN AND PASSWORD ERROR"));
            pw.println("<a href=\"/login?a=exit\">logout</a>");
        }


    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String a = request.getParameter("a");
        HttpSession session = request.getSession(false);

        if ("exit".equals(a) && (session != null))
            session.removeAttribute("user_login");

        response.sendRedirect("index.jsp");
    }
}
