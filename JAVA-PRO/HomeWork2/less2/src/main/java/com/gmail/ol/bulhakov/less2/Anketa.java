package com.gmail.ol.bulhakov.less2;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

public class Anketa extends HttpServlet {
    static final String TEMPLATE = "<html><head></head><body>%s</body></html>";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String lastName = req.getParameter("lastName");
        String age = req.getParameter("age");
        String quest1 = "";
        if(req.getParameter("question1").toLowerCase().equals("yes")){
            quest1 = "Do you like Java  -> yes";
        }else {
            quest1 = "Do you like Java  -> no";
        }
        String quest2 = "";
        if(req.getParameter("question2").toLowerCase().equals("yes")){
            quest2 = "Do you like Java  -> yes";
        }else {
            quest2 = "Do you like Java  -> no";
        }
        resp.getWriter().println(String.format(TEMPLATE,"<div style=\"font-size: 24px\">RESULT QUESTIONNAIRE</div><div>Your name: "+name+"</div><div>Your lastname: "+lastName+"</div>" +
                "<div>Your Age: "+age+"</div><div>"+quest1+"</div><div>"+quest2+"</div>"));
    }
}
